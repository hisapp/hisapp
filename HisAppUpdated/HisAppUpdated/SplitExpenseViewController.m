//
//  SplitExpenseViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "SplitExpenseViewController.h"
#import "DBManager.h"
#import "UIView+firstResponder.h"
#import "MoneyEntryTableViewCell.h"
@interface SplitExpenseViewController ()
{
    UIToolbar* keyboardDoneButtonView , *doneOnly;
    UITextField *activeField;
    NSMutableDictionary *dictManual, *dictDivideByEqual, *dictByRatio;
}
@property(nonatomic) UIBarButtonItem* PrevButton;
@property(nonatomic) UIBarButtonItem*  NextButton;

@property(nonatomic,strong) DBManager *dbManager;
@end

@implementation SplitExpenseViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    [super viewDidLoad];
    //    [self LoadDbData];
    
    dictManual = [[NSMutableDictionary alloc]init];
    dictDivideByEqual = [[NSMutableDictionary alloc]init];
    dictByRatio = [[NSMutableDictionary alloc]init];
    
    
    keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    
    self.PrevButton = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:105 target:self  action:@selector(Previous:)];
    self.NextButton= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:106  target:self action:@selector(Next:)];
    
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* fake = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil] ;
    
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects: self.PrevButton,fake,self.NextButton,fake,flexSpace,fake,doneButton,nil] animated:YES];
    
    
    [self.tblVwSpltList reloadData];
    self.txtConfirmAmount.text = self.strConfirmAmount;
    self.txtConfirmAmount.inputAccessoryView = keyboardDoneButtonView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    [super viewWillDisappear:animated];
}




- (void)Previous:(id)sender
{
    [self switchToNextField:NO];
}

- (void)Next:(id)sender
{
    [self switchToNextField:YES];
}
- (void)switchToNextField:(BOOL)next
{
    if(next)
    {
        
        float tag = activeField.tag - 200;
        NSIndexPath *indexPathUserName = [NSIndexPath indexPathForRow:tag + 1 inSection:0];
        UITableViewCell *cell = (UITableViewCell *)[self.tblVwSpltList cellForRowAtIndexPath:indexPathUserName];
        UITextField *nextTextField = (UITextField *)[cell.contentView viewWithTag:activeField.tag + 1];
        [nextTextField becomeFirstResponder];
    }
    else
    {
        float tag = activeField.tag - 200;
        NSIndexPath *indexPathUserName = [NSIndexPath indexPathForRow:tag - 1 inSection:0];
        UITableViewCell *cell = [self.tblVwSpltList cellForRowAtIndexPath:indexPathUserName];
        UITextField *nextTextField = (UITextField *)[cell.contentView viewWithTag:activeField.tag - 1];
        [nextTextField becomeFirstResponder];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.MembersList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MoneyEntryTableViewCell *cell = (MoneyEntryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UserAmountCell"];
    if (!cell)
    {
        cell = [[MoneyEntryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserAmountCell"] ;
    }
    
    cell.selectionStyle =UITableViewCellSelectionStyleNone;
    cell.userInteractionEnabled = YES;
    
    
    
    UILabel     *lblname  = (UILabel *)    [cell lblMemName];
    lblname.tag =100;
    lblname.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    
    
    UILabel     *lblId  = (UILabel *)    [cell lblmemId];
    lblId.tag =101;
    lblId.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    
    
//    NSLog(@"%ld",(long)indexPath.row);
    
    UITextField *txtfield = (UITextField *)[cell textAmount];
    txtfield.tag = 200 + indexPath.row;
    txtfield.placeholder = @"0.00";
    txtfield.delegate = self;
    [txtfield addTarget:self  action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    txtfield.inputAccessoryView = keyboardDoneButtonView;
    
    if(self.Segment.selectedSegmentIndex ==0)
    {
        if ([dictManual valueForKey:lblId.text] != nil) {
            txtfield.text = [dictManual valueForKey:lblId.text];
        } else {
            txtfield.text = @"";
        }
    }
    
    if(self.Segment.selectedSegmentIndex ==1)
    {
        //        cell.userInteractionEnabled = NO;
        //        float aa = [ self.txtConfirmAmount.text floatValue ] / [self.MembersList count];
        //        txtfield.text = [NSString stringWithFormat:@"%.2f",aa];
        
        if ([dictDivideByEqual valueForKey:lblId.text] != nil) {
            txtfield.text = [dictDivideByEqual valueForKey:lblId.text];
        } else {
            txtfield.text = @"";
        }
    }
    
    
    
    if(self.Segment.selectedSegmentIndex ==2)
    {
        if ([dictByRatio valueForKey:lblId.text] != nil) {
            txtfield.text = [dictByRatio valueForKey:lblId.text];
        } else {
            txtfield.text = @"";
        }
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Whenever you click a cell, bring up a keyboard to edit the textField
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UITextField *textField = (UITextField *)[cell viewWithTag:indexPath.row];
    
    
//    NSLog(@"%@",textField.text);
    [textField becomeFirstResponder];
}

- (IBAction)segentDivideBy:(id)sender {
    switch (self.Segment.selectedSegmentIndex)
    {
        case 0:
        {
            break;
        }
        case 1:
        {
            for(NSArray *aa in self.MembersList)
            {
                float bb = [ self.txtConfirmAmount.text floatValue ] / [self.MembersList count];
                [dictDivideByEqual setObject:[NSString stringWithFormat:@"%.2f",bb] forKey:[aa objectAtIndex:1]];
            }
            break;
        }
        case 2:
        {
            
            break;
        }
        default:
            break;
    }
    
    [self.tblVwSpltList reloadData];
}

#pragma  mark - keyboard And Text field delegate
- (IBAction)doneClicked:(id)sender
{
    [self.tblVwSpltList reloadData];
    [self.view endEditing:YES];
    
}

#pragma mark UIKeyboard handling

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.tblVwSpltList.contentInset = contentInsets;
    self.tblVwSpltList.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.tblVwSpltList scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.tblVwSpltList.contentInset = contentInsets;
    self.tblVwSpltList.scrollIndicatorInsets = contentInsets;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    activeField = textField;
    [self.PrevButton setEnabled:YES];
    [self.NextButton setEnabled:YES];
    if(textField.tag ==200)
        [self.PrevButton setEnabled:NO];
    else if (textField.tag == 200 + [self.MembersList count] - 1)
    {
        [self.NextButton setEnabled:NO];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyDone) {
        [self.view endEditing:YES];
    }
    return YES;
}

-(void)textFieldDidChange:(UITextField *)txtField {
    UILabel *label = (UILabel *)[txtField.superview viewWithTag:101];
    NSString *lblUserId = label.text;
    NSString *txtAmount = txtField.text;
    
    if(self.Segment.selectedSegmentIndex == 0)
    {
        if([txtField.text length]==0)
            [dictManual removeObjectForKey:lblUserId];
        else
            [dictManual setObject:txtAmount forKey:lblUserId];
        
    }
    if(self.Segment.selectedSegmentIndex == 2)
    {
        if([txtField.text length]==0)
            [dictByRatio removeObjectForKey:lblUserId];
        else
            [dictByRatio setObject:txtAmount forKey:lblUserId];
    }
}

- (IBAction)DoneClick:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ProfileId =[defaults objectForKey:@"ContactMobileNo"];
    
    NSString *query =[NSString stringWithFormat:@"INSERT INTO  EVENTEXPENSES(EXPENSENAME,AMOUNT,EXPENSEOWNERID,EVENTID,DIVIDETYPE ) VALUES('%@',%@ ,%@ ,%@,%ld)",self.navigationItem.title,self.txtConfirmAmount.text,ProfileId,self.StrEventId,(long)self.Segment.selectedSegmentIndex];
    [self.dbManager executeQuery:query];
    
    NSArray *arrLastId = [self.dbManager loadDataFromDB:@"select seq from sqlite_sequence where name='EVENTEXPENSES'"];
    NSString * lastId = [[arrLastId objectAtIndex:0] objectAtIndex:0];
    
    if(self.Segment.selectedSegmentIndex == 0)
    {
        NSArray *arr = [dictManual allKeys];
        for(NSString * str in arr)
        {
            query = [NSString stringWithFormat:@"INSERT INTO  EXPENSEDETAIL(PAYER,TO_WHOM,AMOUNT,EXPENSEID,STATUSID) VALUES (%@,%@,%@,%@,1)",[self.PaidbyDetail objectAtIndex:1],str,[dictManual valueForKey:str],lastId];
            [self.dbManager executeQuery:query];
            
            query = [NSString stringWithFormat:@"Update contacts set IN_AMT = IN_AMT + %@ where contactid = %@",[dictManual valueForKey:str],str];
            [self.dbManager executeQuery:query];
        }
    }
    else if(self.Segment.selectedSegmentIndex == 1)
    {
        NSArray *arr = [dictDivideByEqual allKeys];
        for(NSString * str in arr)
        {
            query = [NSString stringWithFormat:@"INSERT INTO  EXPENSEDETAIL(PAYER,TO_WHOM,AMOUNT,EXPENSEID,STATUSID) VALUES (%@,%@,%@,%@,1)",[self.PaidbyDetail objectAtIndex:1],str,[dictDivideByEqual valueForKey:str],lastId];
            [self.dbManager executeQuery:query];
            
            query = [NSString stringWithFormat:@"Update contacts set IN_AMT = IN_AMT + %@ where contactid = %@",[dictDivideByEqual valueForKey:str],str];
            [self.dbManager executeQuery:query];
        }
    }
    else if(self.Segment.selectedSegmentIndex == 2)
    {
        NSArray *arr = [dictByRatio allKeys];
        for(NSString * str in arr)
        {
            query = [NSString stringWithFormat:@"INSERT INTO  EXPENSEDETAIL(PAYER,TO_WHOM,AMOUNT,EXPENSEID,STATUSID) VALUES (%@,%@,%@,%@,1)",[self.PaidbyDetail objectAtIndex:1],str,[dictByRatio valueForKey:str],lastId];
            [self.dbManager executeQuery:query];
            
            query = [NSString stringWithFormat:@"Update contacts set IN_AMT = IN_AMT + %@ where contactid = %@",[dictByRatio valueForKey:str],str];
            [self.dbManager executeQuery:query];
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
