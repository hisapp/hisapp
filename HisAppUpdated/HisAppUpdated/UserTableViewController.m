//
//  UserTableViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 18/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "UserTableViewController.h"
#import "DBManager.h"

#import <AddressBook/AddressBook.h>


@interface UserTableViewController ()
{
    UIBarButtonItem *plusBtn , *DeleteAll;
    
    CFErrorRef *error ;
    ABAddressBookRef addressBook ;
}
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation UserTableViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    
    error = nil;
    addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted) {}
            else {}
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {}
    else {}

    
    plusBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddUserClick:)];
    DeleteAll = [[UIBarButtonItem alloc] initWithTitle:@"Delete All" style:UIBarButtonItemStylePlain target:self action:@selector(AddUserClick:)];
    
    self.navigationItem.rightBarButtonItem = plusBtn;
    [self LoadDbData];
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated
{
//    NSString *query = [NSString stringWithFormat:@"select * from contacts where archived ='no'"];
//    self.arrMembersList = (NSMutableArray *)[self.dbManager loadDataFromDB:query];
//    [self.tableView reloadData];
    [self LoadDbData];
     [self.tableView reloadData];
}



-(void)viewWillDisappear:(BOOL)animated
{
    if(self.tableView.editing)
    {
        [super setEditing:NO animated:YES];
        [self.tableView setEditing:NO animated:YES];
        [self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
        
        self.navigationItem.rightBarButtonItem = plusBtn;
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleBordered];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)LoadDbData
{
    NSString *query;
    if(self.arrMembersList ==nil)
    {
        self.arrMembersList = [[NSMutableArray alloc] init];
    }
    query = [NSString stringWithFormat:@"select * from contacts where archived ='no'"];
    // Load the relevant data.
      self.arrMembersList = (NSMutableArray *)[self.dbManager loadDataByAscending:query index:1];
    self.arrMembersList = [self.arrMembersList mutableCopy];
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (NSInteger)[self.arrMembersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *UserCellIdentifier =@"UserCellIdentifier";
    
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:UserCellIdentifier];
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
//    UIView *cc = (UIView *)[cell.contentView viewWithTag:7];
//    [cc addGestureRecognizer:gestureRecognizer];
    
     // Configure the cell...
    if(cell ==nil)
    {
    }
    
    UILabel *Name = (UILabel *)[cell.contentView viewWithTag:1];
    Name.text = [[self.arrMembersList objectAtIndex:indexPath.row] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"CONTACTNAME"]];
    
    UILabel *hiddenId = (UILabel *)[cell.contentView viewWithTag:2];
    hiddenId.text =[[self.arrMembersList objectAtIndex:indexPath.row] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"CONTACTID"]];
//    cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UILabel *Cont_Id = (UILabel *)[cell.contentView viewWithTag:2];
        
        [self.arrMembersList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        NSString *query = [NSString stringWithFormat:@"Update CONTACTS set archived = 'yes' where CONTACTID=%@", Cont_Id.text];
        [self.dbManager executeQuery:query];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"AddUserContact"]){
        ConatctViewController *controller = (ConatctViewController *)segue.destinationViewController;
        controller.delegate = self;
        controller.TempContact = self.arrMembersList;
    }
    if([segue.identifier isEqualToString:@"AddUser"]){
    }
}



-(void)addItemViewController:(ConatctViewController *)controller didFinishEnteringItem:(NSMutableArray *)SelectedContact
{
//    NSLog(@"%@",SelectedContact);
    
//    self.arrMembersList =SelectedContact;

    

    NSMutableArray *myData = [[NSMutableArray alloc] init];
    for (ContactDetail *cont in SelectedContact) {
        [myData addObject:[cont dictionary]];
    }
    
    NSError *err = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:myData options:NSJSONWritingPrettyPrinted error:&err];
    if ([jsonData length] > 0 &&
        err == nil){
        //        NSLog(@"Successfully serialized the dictionary into data = %@", jsonData);
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        NSLog(@"JSON String = %@", jsonString);
    }
    else if ([jsonData length] == 0 &&
             error == nil){
//        NSLog(@"No data was returned after serialization.");
    }
    else if (error != nil){
        NSLog(@"An error happened = %@", err);
    }
}

- (IBAction)AddUserClick:(id)sender {
    
    
    if([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Delete All"])
    {
        [self.arrMembersList removeAllObjects];
        [self.tableView reloadData];
        
        [self.dbManager executeQuery:@"Update CONTACTS set archived = 'yes'"];
        
        [super setEditing:NO animated:YES];
        [self.tableView setEditing:NO animated:YES];
        
        [self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
        
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddUserClick:)];
        self.navigationItem.rightBarButtonItem = addButton;
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleBordered];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
    else
    {
        
        if ([UIAlertController class]) {
            // use UIAlertController
            
            UIAlertController * view=   [UIAlertController
                                         alertControllerWithTitle:@"Add User"
                                         message:@"Select your Choice"
                                         preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction* AddManually = [UIAlertAction
                                 actionWithTitle:@"Manually enter detail"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [self performSegueWithIdentifier:@"AddUser" sender:self];
                                     //Do some thing here
                                     [view dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            UIAlertAction* FromContact = [UIAlertAction
                                     actionWithTitle:@"From Contact"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [self performSegueWithIdentifier:@"AddUserContact" sender:self];
                                         [view dismissViewControllerAnimated:YES completion:nil];
                                         
                                     }];
            
            
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                            
                                     style:UIAlertActionStyleCancel
                                     handler:^(UIAlertAction * action)
                                     {
                                         [view dismissViewControllerAnimated:YES completion:nil];
                                     }];
            
        
            [view addAction:AddManually];
            [view addAction:FromContact];
            [view addAction:cancel];
            [self presentViewController:view animated:YES completion:nil];
        
        } else {
          UIActionSheet *actionSheet  = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Manually enter detail",@"From Contact",nil];
            [actionSheet setDelegate:self];
            [actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
            
        }
    }
}

- (IBAction)EditUserList:(id)sender {
    
    if([self.arrMembersList count] >0)
    {
        if(self.tableView.editing)
        {
            [super setEditing:NO animated:YES];
            [self.tableView setEditing:NO animated:YES];
            [self.navigationItem.leftBarButtonItem setTitle:@"Edit"];
            [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
            
            self.navigationItem.rightBarButtonItem = plusBtn;
            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleBordered];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
            
        }
        else
        {
            [super setEditing:YES animated:YES];
            [self.tableView setEditing:YES animated:YES];
            [self.navigationItem.leftBarButtonItem setTitle:@"Cancel"];
            [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
            
            self.navigationItem.rightBarButtonItem = DeleteAll;
            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor redColor]];
        }
    }
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
            
        case 0:
        {
            //            alertview =[[UIAlertView alloc] initWithTitle:@"Add Member" message:@"Enter name and Mobile number" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Add", nil];
            //            [alertview setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
            //            alertview.tag =1;
            //            [[alertview textFieldAtIndex:1] setSecureTextEntry:NO];
            //            [[alertview textFieldAtIndex:0] setPlaceholder:@"Name"];
            //            [[alertview textFieldAtIndex:1] setPlaceholder:@"Mobile No. (Only 10 digits)"];
            //            //    [[alertview textFieldAtIndex:2] setPlaceholder:@"type"];
            //            UITextField *tf1 =[alertview textFieldAtIndex:1];
            //            tf1.keyboardType = UIKeyboardTypePhonePad;
            //
            //            if(![strMemberTempName isEqual:[NSNull null]])
            //            {
            //                [alertview textFieldAtIndex:0].text = strMemberTempName;
            //            }
            //            [alertview show];
            //            break;
            [self performSegueWithIdentifier:@"AddUser" sender:self];
            break;
        }
        case 1:
        {
            [self performSegueWithIdentifier:@"AddUserContact" sender:self];
            break;
        }
        default:
            break;
    }
}
@end
