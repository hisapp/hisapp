//
//  SettlementViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 31/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "SettlementViewController.h"
#import "DBManager.h"

@interface SettlementViewController ()
{
    NSString *query;
    NSArray *arr  , *arrHeaders;
}
@property (nonatomic, strong) DBManager *dbManager;

@property(nonatomic, strong) NSMutableDictionary *dictAmount;
@end

@implementation SettlementViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    
    self.dictAmount = [[NSMutableDictionary alloc] init];
    arrHeaders = [NSArray new];
    self.dictAmount = [self.dictAmount mutableCopy];
    
    query = [NSString stringWithFormat:@"SELECT DISTINCT PAYER FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid = %@)",self.StrEventId];
    arrHeaders = [self.dbManager loadDataFromDB:query];
    arrHeaders = [arrHeaders copy];

    
//    SELECT DISTINCT PAYER FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1)
    
    
    //[self loadMoneyResult];
    [super viewDidLoad];
    _scrollView.showsHorizontalScrollIndicator = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    for (UIView *subview in _scrollView.subviews) {
        [subview removeFromSuperview];
    }
    
    
    [self CalculateSettlements];
    [self ShowResult];
    [self LoadMoney_YouOwe];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return [arrHeaders count];
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return [[arrHeaders objectAtIndex:section] objectAtIndex:0] ;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arr count];
//    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *arrUser = [NSArray new];
    query = [NSString stringWithFormat:@"SELECT ContactName from COntacts where contactid  = %@",[arr objectAtIndex:indexPath.row]];
    
    arrUser = [self.dbManager loadDataFromDB:query];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResultCell" forIndexPath:indexPath];
    
    UILabel *lblName = (UILabel *) [cell.contentView viewWithTag:1];
    lblName.text = [[arrUser objectAtIndex:0] objectAtIndex:0];
    
    UILabel *lblAmount = (UILabel *) [cell.contentView viewWithTag:2];
    lblAmount.text =[NSString stringWithFormat:@"%@",[self.dictAmount valueForKey:[arr objectAtIndex:indexPath.row]]];

    UILabel *lblUserId = (UILabel *) [cell.contentView viewWithTag:3];
    lblUserId.text = [NSString stringWithFormat:@"%@",[arr objectAtIndex:indexPath.row]];
    return cell;
}

-(void)CalculateSettlements
{
    query =[NSString stringWithFormat:@"SELECT EXPENSEID FROM EVENTEXPENSES WHERE EVENTID = %@", self.StrEventId];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
    for(NSArray *expenseID in arr)
    {
        query = [NSString stringWithFormat:@"SELECT DISTINCT PAYER FROM EXPENSEDETAIL WHERE EXPENSEID =  %@", [expenseID objectAtIndex:0]];
        NSArray *arrPAYER =[self.dbManager loadDataFromDB:query];
        arrPAYER = [arrPAYER copy];
        
        for(NSArray *payerId in arrPAYER)
        {
            [self.dictAmount setObject:[[NSMutableDictionary alloc]init] forKey:[payerId objectAtIndex:0]];
        }
    }
    
    for(NSString *payerId in [self.dictAmount allKeys])
    {
        query = [NSString stringWithFormat:@"SELECT  TO_WHOM , AMOUNT FROM EXPENSEDETAIL WHERE EXPENSEID in (SELECT EXPENSEID FROM EVENTEXPENSES WHERE EVENTID = %@) and payer = %@",self.StrEventId, payerId];
        arr =[self.dbManager loadDataFromDB:query];
        arr = [arr copy];
        
        NSMutableDictionary* sum = [NSMutableDictionary new];
        for (NSArray* item in arr)
        {
            id key = item[0];
            if (sum[key] == nil)
            {
                sum[key] = @([item[1] floatValue]);
            }
            else
            {
                sum[key] = @([sum[key] floatValue] + [item[1] floatValue]);
            }
        }
        
//        NSLog(@"%@", sum.description);
        
        [self.dictAmount setObject:sum forKey:payerId] ;
    }
    
    //    query =SELECT  to_whom , AMOUNT FROM EXPENSEDETAIL WHERE EXPENSEID = 2 and payer = 9578659012
    //
    //
    //     query =[NSString stringWithFormat:@"SELECT userid FROM USEREVENTREL where  eventid = %@", self.StrEventId];
    //
    //    arr =[self.dbManager loadDataFromDB:query];
    //    arr = [arr copy];
    //
    //    for(NSArray *userID in arr)
    //    {
    //        [self.dictAmount setObject:[[NSMutableDictionary alloc]init] forKey:[userID objectAtIndex:0]];
    //    }
    //    query = [NSString stringWithFormat:@"SELECT EXPENSEID FROM EVENTEXPENSES WHERE EVENTID = %@", self.StrEventId];
    //    arr =[self.dbManager loadDataFromDB:query];
    //    arr = [arr copy];
    //
    //    for(NSArray *expenseID in arr)
    //    {
    //        query = [NSString stringWithFormat:@"SELECT TO_WHOM FROM EXPENSEDETAIL WHERE EXPENSEID =  %@", [expenseID objectAtIndex:0]];
    //        NSArray *arrTO_WHOM =[self.dbManager loadDataFromDB:query];
    //        arrTO_WHOM = [arrTO_WHOM copy];
    //    }
//    NSLog(@"%@",self.dictAmount);
}

-(void)ShowResult
{
    int y1= 0;
    int tag =0;
    for(NSString *payerId in [self.dictAmount allKeys])
    {
        tag++;
        query = [NSString stringWithFormat:@"SELECT CONTACTNAME FROM CONTACTS WHERE CONTACTID = %@",payerId];
        arr =[self.dbManager loadDataFromDB:query];
        arr = [arr copy];
        
        UILabel *PayerName =[[UILabel alloc] initWithFrame:CGRectMake(10, y1, 250, 30)];
        PayerName.tag =tag;
        PayerName.text = [NSString stringWithFormat:@"%@",[[arr objectAtIndex:0] objectAtIndex:0]];
        [_scrollView addSubview:PayerName];
        
        y1+=30;
        
        NSMutableArray *dictSample = [[NSMutableArray alloc]init];
        [dictSample addObject:[self.dictAmount objectForKey:payerId]];

        for(NSDictionary *dict in dictSample)
        {
            for(NSString *ToWhomID in [dict allKeys])
            {
                if([payerId isEqualToString:ToWhomID])
                {
                    continue;
                }
                tag++;
                query = [NSString stringWithFormat:@"SELECT  CONTACTNAME FROM CONTACTS WHERE CONTACTID = %@",ToWhomID];
                arr =[self.dbManager loadDataFromDB:query];
                arr = [arr copy];
                
                UILabel *ToWhomName =[[UILabel alloc] initWithFrame:CGRectMake(20, y1+10, 200, 30)];
                ToWhomName.tag =tag;
           
                ToWhomName.text =[NSString stringWithFormat:@"%@",[[arr objectAtIndex:0]  objectAtIndex:0]];
          
                [_scrollView addSubview:ToWhomName];
                
                UILabel *Amount =[[UILabel alloc] initWithFrame:CGRectMake(210, y1+10, 250, 30)];
                Amount.tag =tag +1;
                Amount.text = [NSString stringWithFormat:@"%@",[dict valueForKey:ToWhomID]];
                [_scrollView addSubview:Amount];
                y1+=30;
            }
        }
        y1+=30;
    }
     [_scrollView setContentSize:CGSizeMake(self.view.bounds.size.width,y1)];
}

-(void)LoadMoney_YouOwe
{
    
    query = [NSString stringWithFormat:@"SELECT userid from USEREVENTREL where eventid = %@",self.StrEventId];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
    
    self.dictAmount = [NSMutableDictionary new];
    for (NSArray* item in arr)
    {
        id key = item[0];
        if (self.dictAmount[key] == nil)
        {
            self.dictAmount[key] = @(0.00);
        }
    }

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ProfileId =[defaults objectForKey:@"ContactMobileNo"];
    
    query = [NSString stringWithFormat:@" SELECT EXPENSEID ,amount , to_whom FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid= %@)  and  payer = %@",self.StrEventId,ProfileId];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
    
    for (NSArray* item in arr)
    {
        id key = item[2];
        if (self.dictAmount[key] == nil)
        {
            self.dictAmount[key] = @(0.00);
        }
        else
        {
            self.dictAmount[key] = @([self.dictAmount[key] floatValue] + [item[1] floatValue]);
        }
    }
    
    query = [NSString stringWithFormat:@"SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=%@) and Payer <> %@ and to_whom = %@",self.StrEventId,ProfileId,ProfileId];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
    
    for (NSArray* item in arr)
    {
        id key = item[2];
        if (self.dictAmount[key] == nil)
        {
            self.dictAmount[key] = @(0.00);
        }
        else
        {
            self.dictAmount[key] = @([self.dictAmount[key] floatValue] - [item[1] floatValue]);
        }
    }
    
    arr = [self.dictAmount allKeys];
    arr = [arr copy];
    
    
//     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1) and Payer <> 9578659012 and to_whom = 9578659012
//    for(NSArray *owe in arr)
//    {
//         query = [NSString stringWithFormat:@" SELECT  EXPENSEID , amount , payer  FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=%@) and Payer = %@ and to_whom =  %@",self.StrEventId,ProfileId,ProfileId];
        
        
//         SELECT expenseid, to_whom , amount , payer  FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1 ) and Payer = 9578659012 and to_whom <>  9578659012
//    }
//    owe
//     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE  Payer <> 9578659012 and to_whom =9578659012
    
//    you are  owed
    
//     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1) and Payer = 9578659012 and to_whom = owe payers
    
}

-(void)loadMoneyResult
{
    NSArray *arrExpenses =[self.dbManager loadDataFromDB:@"SELECT EXPENSEID ,EXPENSENAME ,AMOUNT FROM EVENTEXPENSES WHERE EVENTID = 1"];
    
    arrExpenses = [arrExpenses copy];
    int y1= 0;
    int tag =0;
    for(NSArray *ar in arrExpenses)
    {
//        NSString *Query = [NSString stringWithFormat:@"SELECT * FROM EXPENSEDETAIL WHERE EXPENSEID  = %@",[ar objectAtIndex:0]];
//        NSArray *SingleExpense =[self.dbManager loadDataFromDB:Query];
        
        UILabel *ExpenseName =[[UILabel alloc] initWithFrame:CGRectMake(10, y1+10, 250, 50)];
        //[quest setFrame:CGRectMake(10.0, qy, 250.0, 300)];
        ExpenseName.tag =tag;
        ExpenseName.text =[ar objectAtIndex:1];
        [_scrollView addSubview:ExpenseName];
        
        //        UILabel *PaidBy =[[UILabel alloc] initWithFrame:CGRectMake(10, y1+10, 250, 50)];
        //        //[quest setFrame:CGRectMake(10.0, qy, 250.0, 300)];
        //        PaidBy.tag =tag;
        //        PaidBy.text =[dict_1_event objectForKey:@"PaidBy"];
        //        [_scrlVwResult addSubview:PaidBy];
        //
        //
        //
        //        NSArray *arr1 = [[dict_1_event objectForKey:@"PaidFor"] allKeys];
        //        NSDictionary *dict1= [dict_1_event objectForKey:@"PaidFor"];
        y1+=50;
        tag++;
        //        for(int count =0; count< [arr1 count] ;count++)
        //        {
        //            UILabel *lblAmount = [[UILabel alloc] initWithFrame:CGRectMake(50, y1+10, 250, 40)];
        //            lblAmount.tag =tag;
        //            lblAmount.text =[NSString stringWithFormat:@"%@ -> %@ ",[arr1 objectAtIndex:count] ,[dict1 objectForKey:[arr1 objectAtIndex:count]]];
        //            [_scrlVwResult addSubview:lblAmount];
        //            y1+=40;
        //            tag++;
        //        }
    }
    [_scrollView setScrollEnabled:YES];
    [_scrollView setContentSize:CGSizeMake(self.view.bounds.size.width,y1)];
}


-(void)LoadMoney_All
{
    self.dictAmount = [NSMutableDictionary new];
    
    for(NSArray *payer in arrHeaders)
    {
        id key = payer[0];
        if (self.dictAmount[key] == nil)
        {
            [self.dictAmount  setObject:[NSMutableDictionary new] forKey:key];
//            self.dictAmount[key] = [NSMutableDictionary new];
        }
    }
    
    
    query = [NSString stringWithFormat:@"SELECT userid from USEREVENTREL where eventid = %@",self.StrEventId];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
    
    
    for(NSString *str in [self.dictAmount allKeys])
    {
        for (NSArray* item in arr)
        {
            id key = item[0];
            [[self.dictAmount valueForKey:str] setObject:@(0.00) forKey:key];
        }
    }
    
   
    
    
for(NSString *str in [self.dictAmount allKeys])
{
    query = [NSString stringWithFormat:@"SELECT  amount ,to_whom FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=%@) and payer = %@",self.StrEventId,str];
    arr =[self.dbManager loadDataFromDB:query];
    arr = [arr copy];
        
    for (NSArray* item in arr)
    {
        id key = item[1];
        if (self.dictAmount[str][key] == nil)
        {
            self.dictAmount[str][key] = @(0.00);
        }
        else
        {
            self.dictAmount[str][key] = @([self.dictAmount[str][key] floatValue] + [item[0] floatValue]);
        }
    }
}
    
     NSLog(@" %@",self.dictAmount);
    
    
    
    arr = [self.dictAmount allKeys];
    arr = [arr copy];
    
    
    
    
    
    
    
    
    
    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *ProfileId =[defaults objectForKey:@"ContactMobileNo"];
//    
//    query = [NSString stringWithFormat:@" SELECT EXPENSEID ,amount , to_whom FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid= %@)  and  payer = %@",self.StrEventId,ProfileId];
//    arr =[self.dbManager loadDataFromDB:query];
//    arr = [arr copy];
//    
//    for (NSArray* item in arr)
//    {
//        id key = item[2];
//        if (self.dictAmount[key] == nil)
//        {
//            self.dictAmount[key] = @(0.00);
//        }
//        else
//        {
//            self.dictAmount[key] = @([self.dictAmount[key] floatValue] + [item[1] floatValue]);
//        }
//    }
//    
//    query = [NSString stringWithFormat:@"SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=%@) and Payer <> %@ and to_whom = %@",self.StrEventId,ProfileId,ProfileId];
//    arr =[self.dbManager loadDataFromDB:query];
//    arr = [arr copy];
//    
//    
//    for (NSArray* item in arr)
//    {
//        id key = item[2];
//        if (self.dictAmount[key] == nil)
//        {
//            self.dictAmount[key] = @(0.00);
//        }
//        else
//        {
//            self.dictAmount[key] = @([self.dictAmount[key] floatValue] - [item[1] floatValue]);
//        }
//    }
//    
//    arr = [self.dictAmount allKeys];
//    arr = [arr copy];
    
    
    //     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1) and Payer <> 9578659012 and to_whom = 9578659012
    //    for(NSArray *owe in arr)
    //    {
    //         query = [NSString stringWithFormat:@" SELECT  EXPENSEID , amount , payer  FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=%@) and Payer = %@ and to_whom =  %@",self.StrEventId,ProfileId,ProfileId];
    
    
    //         SELECT expenseid, to_whom , amount , payer  FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1 ) and Payer = 9578659012 and to_whom <>  9578659012
    //    }
    //    owe
    //     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE  Payer <> 9578659012 and to_whom =9578659012
    
    //    you are  owed
    
    //     SELECT  EXPENSEID ,amount ,payer FROM EXPENSEDETAIL WHERE ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1) and Payer = 9578659012 and to_whom = owe payers
    
}


- (IBAction)OweSegmentClick:(id)sender {
    
    switch (self.OweSegment.selectedSegmentIndex)
    {
        case 0:
        {
            
            
            break;
        }
        case 1:
        {
//            [self LoadMoney_YouOwe];
//            break;
        }
        case 2:
        {
            
//            [self LoadMoney_All];
//            SELECT  *  FROM EXPENSEDETAIL WHERE  ExpenseId in (Select expenseid from EVENTEXPENSES)  and to_whom =9578659012
            
//            SELECT  *  FROM EXPENSEDETAIL WHERE  ExpenseId in (Select expenseid from EVENTEXPENSES where eventid=1)  and to_whom =9578659012
            
//            SELECT  EXPENSEID   FROM EXPENSEDETAIL WHERE to_whom =9578659012
            
//            SELECT  EXPENSEID   FROM EXPENSEDETAIL WHERE  Payer = 9578659012 and to_whom =9578659012
            
//            SELECT  amount   FROM EXPENSEDETAIL WHERE  Payer = 9578659012 and to_whom =9578659012
            break;
        }
        default:
            break;
    }
//    [self.tblResult reloadData];
}



@end
