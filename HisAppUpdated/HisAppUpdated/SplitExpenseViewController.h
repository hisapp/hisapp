//
//  SplitExpenseViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitExpenseViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *txtConfirmAmount;
@property (strong, nonatomic) IBOutlet UITableView *tblVwSpltList;
@property (strong, nonatomic) IBOutlet UISegmentedControl *Segment;

- (IBAction)DoneClick:(id)sender;
- (IBAction)segentDivideBy:(id)sender;


@property (strong, nonatomic)          NSString *strConfirmAmount;
@property(nonatomic,strong) NSString *StrEventId;
@property(nonatomic ,strong) NSArray *PaidbyDetail;
@property(nonatomic, strong) NSMutableArray *MembersList;
@end
