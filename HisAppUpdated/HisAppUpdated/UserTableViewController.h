//
//  UserTableViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 18/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactDetail.h"
#import "ConatctViewController.h"
#import "AddUserViewController.h"

@interface UserTableViewController : UITableViewController<UIActionSheetDelegate,SendContactDetail>

- (IBAction)AddUserClick:(id)sender;
- (IBAction)EditUserList:(id)sender;

@property(nonatomic) ContactDetail *contacts;
@property(nonatomic,strong) NSMutableArray *arrMembersList;
@end
