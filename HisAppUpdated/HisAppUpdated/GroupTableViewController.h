//
//  GroupTableViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 17/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface GroupTableViewController : UITableViewController


@property(nonatomic,strong) NSMutableArray *arrGroupsList;
@end
