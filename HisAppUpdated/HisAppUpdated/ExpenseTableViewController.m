//
//  ExpenseTableViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "ExpenseTableViewController.h"
#import "AddExpenseViewController.h"
#import "DBManager.h"

@interface ExpenseTableViewController ()

@property(nonatomic, strong) NSMutableArray *Expense;
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation ExpenseTableViewController

- (void)viewDidLoad {
    
    
    self.dbManager = [DBManager sharedObject];
   self.ExpenseDetail = [[NSMutableArray alloc] init];

    [super viewDidLoad];
    [self LoadDbData];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self LoadDbData];
    [self.tableView reloadData];
}


-(void)LoadDbData
{
    if(self.ExpenseList ==nil)
    {
        self.ExpenseList = [[NSMutableArray alloc] init];
    }
    NSString *query = [NSString stringWithFormat:@"select * from EVENTEXPENSES WHERE EVENTID = %@", self.StrEventId];
    self.ExpenseList = (NSMutableArray *)[self.dbManager loadDataByAscending:query index:1];
    self.Expense = [self.ExpenseList copy];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.Expense count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExpenseCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    
    // SELECT DISTINCT PAYER  FROM EXPENSEDETAIL WHERE EXPENSEID  = 2
    if(cell == nil)
    {
        
    }
    

    
    UILabel *lblexpenseName = (UILabel *)[cell.contentView viewWithTag:1];
    lblexpenseName.text = [[self.Expense objectAtIndex:indexPath.row] objectAtIndex:1];
    
    UILabel *LblExpenseId =(UILabel *)[cell.contentView viewWithTag:2];
    LblExpenseId.text = [[self.Expense objectAtIndex:indexPath.row] objectAtIndex:0];
    
    self.ExpenseDetail = (NSMutableArray *)[self.dbManager loadDataFromDB:[NSString stringWithFormat:@"SELECT CONTACTNAME FROM CONTACTS WHERE CONTACTID IN (SELECT  PAYER  FROM EXPENSEDETAIL WHERE EXPENSEID  = %@ )", [[self.Expense objectAtIndex:indexPath.row] objectAtIndex:0]]];
    
    UILabel *lblPaidBy =(UILabel *)[cell.contentView viewWithTag:3];
    
    lblPaidBy.attributedText = [self AttributeStringBold:@"Paid By" val:[[self.ExpenseDetail objectAtIndex:0] objectAtIndex:0]];
    
    UILabel *lblAmount =(UILabel *)[cell.contentView viewWithTag:4];
    lblAmount.attributedText =[self AttributeStringBold:@"Rs." val:[[self.Expense objectAtIndex:indexPath.row] objectAtIndex:2]];
  
    
   self.ExpenseDetail = (NSMutableArray *) [self.dbManager loadDataFromDB:[NSString stringWithFormat:@" SELECT CONTACTNAME FROM CONTACTS WHERE CONTACTID IN (SELECT TO_WHOM  FROM EXPENSEDETAIL WHERE EXPENSEID  = %@)", [[self.Expense objectAtIndex:indexPath.row] objectAtIndex:0]]];

    NSString *PaidFor =@"";
    NSMutableArray *arrmem = [[NSMutableArray alloc] init];
    
    for(NSArray *pp in self.ExpenseDetail)
    {
        [arrmem addObject:[pp objectAtIndex:0]];
    }
    PaidFor = [arrmem componentsJoinedByString:@","];

    UILabel *lblPaidFor =(UILabel *)[cell.contentView viewWithTag:5];
    lblPaidFor.attributedText =  [self AttributeStringBold:@"Paid For" val:PaidFor];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"AddExpense"])
    {
        UINavigationController *navigation = (UINavigationController *)segue.destinationViewController;
        AddExpenseViewController *AddExpense = (AddExpenseViewController *)navigation.topViewController;
        AddExpense.StrEventId =self.StrEventId;
    }
}



- (IBAction)Home_CloseClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)AddExpenseClick:(id)sender {
    
    [self performSegueWithIdentifier:@"AddExpense" sender:self];
}


-(NSMutableAttributedString *)AttributeStringBold :(NSString *)lblStr val:(NSString *)valStr
{
    NSString *str = [NSString stringWithFormat:@"%@ : %@",lblStr,valStr];
    NSRange range1 = [str rangeOfString:[NSString stringWithFormat:@"%@ :",lblStr]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:14.0]} range:range1];
    
    return  attributedText;
}
@end
