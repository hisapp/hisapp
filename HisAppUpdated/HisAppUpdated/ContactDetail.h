//
//  ContactDetail.h
//  Hisaab
//
//  Created by GAYATHIRIDEVI on 27/02/15.
//  Copyright (c) 2015 GAYATHIRIDEVI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactDetail : NSObject

@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *fullName;

@property (nonatomic) NSString *mobileNumber;
@property (nonatomic) NSMutableArray *mobileNumbers;

@property (nonatomic) NSString *Email;
@property (nonatomic) NSMutableArray *Emails;


@property (assign) bool Isopen;
@property (assign) bool IsChecked;

-(NSDictionary *)dictionary;
-(NSString *)Stringvanish:(NSString *)incorrect;
@end
