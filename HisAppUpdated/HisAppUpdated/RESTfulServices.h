//
//  RESTfulServices.h
//  SPFFoods
//
//  Created by DURGA PRASAD on 22/02/14.
//  Copyright (c) 2014 SPFSolutions, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RestfulDelegate <NSObject>

@required

- (void)RestServiceLoadedData:(NSMutableData*)data forIdentifier:(NSString*)identifier;
- (void)RestServiceFailedwithError:(NSError*)err forIdentifier:(NSString*)identifier;

@end


@interface RESTfulServices : NSObject
{
    NSURLConnection *connection;
    NSMutableData   *responseData;
    
    __unsafe_unretained id<RestfulDelegate> delegate;
}

@property (nonatomic, retain)    NSString        *serviceIdentifier;
@property (nonatomic, retain)    NSMutableData   *responseData;

-(void)setDelegate:(id<RestfulDelegate>)_delegate;

- (void)callServicePost:(NSMutableDictionary*)dict forPath:(NSString*)strPath;
- (void)callServiceGet:(NSString*)method forPath:(NSString*)serverpath;
@end