//
//  GlobalVar.m
//  Game
//
//  Created by GAYATHIRIDEVI on 10/09/14.
//  Copyright (c) 2014 GAYATHIRIDEVI. All rights reserved.
//

#import "GlobalVar.h"

static GlobalVar *gObject;

@implementation GlobalVar

@synthesize SelectedId;                             //int
@synthesize arrOfTotalTripName;       //Nsmutablearray
@synthesize dictAllEvent,dictAllGroups;                           //NsMutableDictionary
@synthesize dropDownStatus;                 //BOOL
@synthesize ProfileId;

+ (GlobalVar*)sharedObject
{
    if(!gObject)
    {
        gObject = [[GlobalVar alloc] init];
    }
    return gObject;
}


+(void)PrintJson:(NSMutableArray *)pArr
{
    NSError *error = nil;
    NSData *json;
    if ([NSJSONSerialization isValidJSONObject:pArr])
    {
        json = [NSJSONSerialization dataWithJSONObject:pArr options:NSJSONWritingPrettyPrinted error:&error];
        if (json != nil && error == nil)
        {
            NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            NSLog(@"JSON: %@", jsonString);
        }
    }
}

-(NSString *)Stringvanish:(NSString *)incorrect
{
    
    NSString *unfilteredString = incorrect;
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *resultString = [[unfilteredString componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    return resultString;
}


@end