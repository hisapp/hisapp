//
//  DemoTableControllerViewController.m
//  FPPopoverDemo
//
//  Created by Alvise Susmel on 4/13/12.
//  Copyright (c) 2012 Fifty Pixels Ltd. All rights reserved.
//

#import "DemoTableController.h"
#import "AddExpenseViewController.h"
@interface DemoTableController ()

@end

@implementation DemoTableController
@synthesize delegate=_delegate;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Paid By";
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.MembersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
        UILabel *lblName = [[UILabel alloc] init];
        [lblName setTag:1];
        [lblName setFont:[UIFont boldSystemFontOfSize:14]];
        [lblName setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lblName];
        
        UILabel *lbl_Id = [[UILabel alloc] init];
        [lbl_Id setTag:2];
        [lbl_Id setFont:[UIFont boldSystemFontOfSize:14]];
        [lbl_Id setBackgroundColor:[UIColor clearColor]];
        [cell.contentView addSubview:lbl_Id];
    }
    
    
    UILabel *name = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *IDno = (UILabel *)[cell.contentView viewWithTag:2];
    
    
    name.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    IDno.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    
    [name setFrame:CGRectMake(10.0, 5.0, self.tableView.bounds.size.width - 10,20.0)];
    IDno.hidden = YES;
    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblUserNAme = (UILabel *) [cell.contentView viewWithTag:1];
    UILabel *lblUserID = (UILabel *) [cell.contentView viewWithTag:2];
    
    NSLog(@"%@__________%@",lblUserNAme.text,lblUserID.text);
    
    if([self.delegate respondsToSelector:@selector(selectedTableRow:)])
    {
        [self.delegate selectedTableRow:[self.MembersList objectAtIndex:indexPath.row]];
    }
}




@end
