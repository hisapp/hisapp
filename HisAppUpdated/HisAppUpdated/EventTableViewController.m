//
//  EventTableViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 25/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "EventTableViewController.h"
#import "DBManager.h"
#import "EventUserTableViewController.h"
#import "SettlementViewController.h"

@interface EventTableViewController ()
{
    NSString *EventUserNavBarTitle, *EventId;
}

@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation EventTableViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    [self LoadDbData];

    [super viewDidLoad];
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self LoadDbData];
}


-(void)LoadDbData
{
    if(self.MembersList ==nil)
    {
        self.MembersList = [[NSMutableArray alloc] init];
    }
    NSString *query = [NSString stringWithFormat:@"select * from EVENTS"];
    self.MembersList = (NSMutableArray *)[self.dbManager loadDataByAscending:query index:1];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.MembersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventCell" forIndexPath:indexPath];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventCell"];
        NSLog(@"New Cell Cretaed");
    }
    
    UILabel *lblEventID = (UILabel *) [cell.contentView viewWithTag:2];
    lblEventID.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    
    UILabel *lblEventName = (UILabel *) [cell.contentView viewWithTag:1];
    lblEventName.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.MembersList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UILabel *lblEventID = (UILabel *) [cell.contentView viewWithTag:2];
    
    EventId = lblEventID.text;

    NSString *query = [NSString stringWithFormat:@"select * from EVENTS where eventid = %@", lblEventID.text];
    NSArray *aa =[self.dbManager loadDataFromDB:query];
    EventUserNavBarTitle = [[aa objectAtIndex:0] objectAtIndex:1];
    [self performSegueWithIdentifier:@"EventDetail" sender:self];
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"EventDetail"])
    {
        //if sent to First Tab of Tab Bar Controller
        
        UITabBarController *tabcontroller = (UITabBarController *)segue.destinationViewController;
        
        UINavigationController *ExpensenavigationController = (UINavigationController *)[[tabcontroller viewControllers] objectAtIndex:0];
        EventUserTableViewController *ExpenseListcontroller = (EventUserTableViewController *)ExpensenavigationController.topViewController;
        ExpenseListcontroller.navigationItem.title = EventUserNavBarTitle;
        ExpenseListcontroller.StrEventId = EventId;
        
        UINavigationController *UsernavigationController = (UINavigationController *)[[tabcontroller viewControllers] objectAtIndex:1];
        EventUserTableViewController *UserListcontroller = (EventUserTableViewController *)UsernavigationController.topViewController;
        UserListcontroller.navigationItem.title = EventUserNavBarTitle;
        UserListcontroller.StrEventName = EventUserNavBarTitle;
        UserListcontroller.StrEventId = EventId;
        
        
        UINavigationController *SettlementnavigationController = (UINavigationController *)[[tabcontroller viewControllers] objectAtIndex:2];
        SettlementViewController *Settlementcontroller = (SettlementViewController *)SettlementnavigationController.topViewController;
        Settlementcontroller.navigationItem.title = EventUserNavBarTitle;
        Settlementcontroller.StrEventName = EventUserNavBarTitle;
        Settlementcontroller.StrEventId = EventId;
        
        
    }
}


@end
