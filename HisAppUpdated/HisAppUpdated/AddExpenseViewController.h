//
//  AddExpenseViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NIDropDown.h"


@interface AddExpenseViewController : UIViewController< NIDropDownDelegate>
{
NIDropDown *dropDown;
}
- (IBAction)CloseClick:(id)sender;
- (IBAction)NextClick:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtExpenseName;
@property (strong, nonatomic) IBOutlet UITextField *txtPaidBy;
@property (strong, nonatomic) IBOutlet UITextField *txtAmount;


@property (strong, nonatomic) IBOutlet UIButton *PaiByPop;
- (IBAction)PaidByPopClick:(id)sender;


@property (strong, nonatomic) IBOutlet UIImageView *ImgVwTick;
@property (strong, nonatomic) IBOutlet UIButton *btnTck;
- (IBAction)btnTickClick:(id)sender;
- (IBAction)btnSelectAll:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tblVwListUser;


@property(nonatomic,strong) NSString *StrEventId;
@property(nonatomic,strong) NSArray *PaidByDetail;

-(void)selectedTableRow:(NSArray*)Paidby;
@property(nonatomic, strong) NSMutableArray *MembersList;

@end
