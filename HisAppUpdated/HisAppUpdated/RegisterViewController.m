//
//  RegisterViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 09/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//
//
#import "RegisterViewController.h"

@interface RegisterViewController ()
{
    UITextField *activeField;
    CGSize kbSize;
    CGRect aRect;
    AppDelegate *ApDelegate;
}
@property(nonatomic) float KeyBoardSizeHeight;
@property(nonatomic) UIBarButtonItem* PrevButton;
@property(nonatomic) UIBarButtonItem*  NextButton;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    ApDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.btnPolicy.titleLabel.attributedText = [self AttributeStringBold:1];
    
    [super viewDidLoad];
    
    self.dbManager = [DBManager sharedObject];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    
    self.PrevButton = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:105 target:self  action:@selector(Previous:)];
    self.NextButton= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:106  target:self action:@selector(Next:)];
    
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem* fake = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil] ;
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(KeyBoardDoneClicked:)];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects: self.PrevButton,fake,self.NextButton,fake,flexSpace,fake,doneButton,nil] animated:YES];
    
    
    self.txtFieldName.inputAccessoryView = keyboardDoneButtonView;
    self.txtFieldMobile.inputAccessoryView = keyboardDoneButtonView;
    self.txtFieldEmail.inputAccessoryView = keyboardDoneButtonView;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self deregisterFromKeyboardNotifications];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark Validation Functions

- (BOOL) validateName:(NSString *)text {
    NSString *Regex = @"[A-Za-z0-9 ]*";
    NSPredicate *TestResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
    return [TestResult evaluateWithObject:text];
}

-(BOOL)ValidatePhoneNumber:(NSString *)text
{
    if([self.txtFieldMobile.text length] == 10)
    {
        NSString *Regex = @"[0-9^]*";
        NSPredicate *TestResult = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", Regex];
        return [TestResult evaluateWithObject:text];
    }
    else
    {
        return NO;
    }
}

- (BOOL)validateEmail:(NSString *)inputText {
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    if([emailTest evaluateWithObject:inputText]) {
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        int indexOfDot =(int)aRange.location;
        if(aRange.location != NSNotFound) {
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            NSSet *TLD;
            TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", nil];
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) {
                //NSLog(@"TLD contains topLevelDomain:%@",topLevelDomain);
                return TRUE;
            }
            /*else {
             NSLog(@"TLD DOEST NOT contains topLevelDomain:%@",topLevelDomain);
             }*/
            
        }
    }
    return FALSE;
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // verify the text field you wanna validate
    if (textField == self.txtFieldName) {
        
        // do not allow the first character to be space | do not allow more than one space
        if ([string isEqualToString:@" "]) {
            if (!textField.text.length)
            {
                return NO;
            }
            if ([[textField.text stringByReplacingCharactersInRange:range withString:string] rangeOfString:@"  "].length)
            {
                return NO;
            }
        }
        
        // allow backspace
        if ([textField.text stringByReplacingCharactersInRange:range withString:string].length < textField.text.length) {
            return YES;
        }
        
        // in case you need to limit the max number of characters
        if ([textField.text stringByReplacingCharactersInRange:range withString:string].length > 25) {
            
            return NO;
        }
        
        // limit the input to only the stuff in this character set, so no emoji or cirylic or any other insane characters
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890  _"];
        
        if ([string rangeOfCharacterFromSet:set].location == NSNotFound) {
            return NO;
        }
        
        return YES;
    }
    
    return YES;
}

-(NSMutableAttributedString *)AttributeStringBold:(int)value
{
    NSString *str = [NSString stringWithFormat:@"By Clicking Join In HisApp. You accepted our policy."];
    NSRange range1 = [str rangeOfString:[NSString stringWithFormat:@"policy"]];
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:str];
    if(value ==1)
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blueColor]} range:range1];
    else
    {
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor brownColor]} range:range1];
    }
    return  attributedText;
}
#pragma mark UIKeyboard handling


- (void)Previous:(id)sender
{
    [self.NextButton setEnabled:YES];
    UIView *responder = [self.view findFirstResponder];
    if (nil == responder || ![responder isKindOfClass:[UITextField class]]) {
        return;
    }
    if(responder.tag ==1)
    {
        [self.txtFieldName becomeFirstResponder];
        [self.PrevButton setEnabled:NO];
        activeField = self.txtFieldName;
    }
    else if(responder.tag ==2)
    {
        [self.txtFieldName becomeFirstResponder];
        [self.PrevButton setEnabled:NO];
        activeField = self.txtFieldName;
    }
    else if (responder.tag ==3)
    {
        [self.txtFieldEmail becomeFirstResponder];
        activeField =  self.txtFieldEmail;
    }
}

- (void)Next:(id)sender
{
    [self.PrevButton setEnabled:YES];
    UIView *responder = [self.view findFirstResponder];
    if (nil == responder || ![responder isKindOfClass:[UITextField class]]) {
        return;
    }
    if(responder.tag ==1)
    {
        [self.txtFieldEmail becomeFirstResponder];
        activeField =  self.txtFieldEmail;
    }
    else if (responder.tag ==2)
    {
        [self.txtFieldMobile becomeFirstResponder];
        [self.NextButton setEnabled:NO];
        activeField =  self.txtFieldMobile;
    }
    else if (responder.tag ==3)
    {
        [self.txtFieldMobile becomeFirstResponder];
        activeField =  self.txtFieldMobile;
    }
}


- (IBAction)KeyBoardDoneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height +10.0, 0.0);
    self.ScrollViewContainer.contentInset = contentInsets;
    self.ScrollViewContainer.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    aRect = self.view.frame;
    aRect.size.height -= kbSize.height+10.0;

    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.ScrollViewContainer scrollRectToVisible:activeField.frame animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollViewContainer.contentInset = contentInsets;
    self.ScrollViewContainer.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeField = textField;
    [self.PrevButton setEnabled:YES];
    [self.NextButton setEnabled:YES];
    if(textField.tag ==1)
        [self.PrevButton setEnabled:NO];
    else if (textField.tag ==3)
        [self.NextButton setEnabled:NO];
    
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
        activeField = (UITextField *)next;
    } else if (textField.returnKeyType==UIReturnKeyDone) {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
#pragma mark NSURLConnection Delegate Methods


-(void)RestServiceLoadedData:(NSMutableData *)data forIdentifier:(NSString *)identifier
{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    NSLog(@"Registrion Response------%@",json);
    
    if([identifier isEqualToString:@"SignIn"])
    {
        if([[json allKeys]containsObject:@"registration"])
        {
            [self.navigationItem setHidesBackButton:NO animated:YES];
            [ApDelegate HideProgressView];
            if([[json valueForKeyPath:@"registration.message"] isEqualToString:@"Registration Success"])
            {
                [self AlertView:@"Registration Successful. Enter Verification code that send to your e-mail address." strTitle:@"Welcome To HisApp" isStatus:YES];
            }
            else if ([[json valueForKeyPath:@"registration.message"] isEqualToString:@"User Already Exist"] )
            {
                [self AlertView:@"Enter Verification code that send to your e-mail address." strTitle:@"Welcome Back to HisApp" isStatus:YES];
            }
            NSString *UserId = [json valueForKeyPath:@"registration.userid"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:self.txtFieldName.text forKey:@"Name"];
            [defaults setObject:self.txtFieldMobile.text forKey:@"ContactMobileNo"];
            [defaults setObject:UserId forKey:@"UserId"];
            [defaults setObject:@"nazirjaleel@gmail.com" forKey:@"EmailId"];
            [defaults synchronize];
            NSString *strQuery = [NSString stringWithFormat:@"Insert OR IGNORE into contacts(contactID,contactName,contactNo,contactEmail,archived )values(%@,'%@','%@','%@','yes')",UserId,self.txtFieldName.text,self.txtFieldMobile.text,self.txtFieldEmail.text];
            [self.dbManager executeQuery:strQuery];
        }
        else
        {
            [self AlertView:@"Sorry.Something goes wrong." strTitle:@"Error" isStatus:NO];
        }
    }
    else if([identifier isEqualToString:@"VerifyCode"])
    {
        if([[json allKeys]containsObject:@"verification"])
        {
            if([[json valueForKeyPath:@"verification.message"] isEqualToString:@"Verification Success"])
            {
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"SignUp" forKey:@"SignUp"];
                [defaults synchronize];
                [self performSegueWithIdentifier:@"SyncUpToHome" sender:self];
            }
        }
        else if([[json allKeys]containsObject:@"error"])
        {
            if([[json valueForKeyPath:@"error.message"] isEqualToString:@"Already Registered"])
            {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:@"SignUp" forKey:@"SignUp"];
                [defaults synchronize];
                [self performSegueWithIdentifier:@"SyncUpToHome" sender:self];
            }
            else if ([[json valueForKeyPath:@"error.message"] isEqualToString:@"Invalid key"])
            {
                [self AlertView:@"Sorry. You entered wrong code." strTitle:@"Wrong verification Code" isStatus:NO];
            }
        }
    }
}

-(void)RestServiceFailedwithError:(NSError *)err forIdentifier:(NSString *)identifier
{
    
}

#pragma mark AlertView

-(void)AlertView:(NSString *)Message  strTitle:(NSString*)title isStatus:(BOOL)Success
{
    if ([UIAlertController class]) {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:title
                                      message:Message
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 if(Success)
                                 {
                                     [self performSegueWithIdentifier:@"VerifyCode" sender:self];
                                 }
                             }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title  message:Message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        if(Success)
        {
            [self performSegueWithIdentifier:@"VerifyCode" sender:self];
        }
    }
}

#pragma mark Button Click Functions

- (IBAction)Register:(id)sender {
    
    [self.dbManager executeQuery:@"PRAGMA foreign_keys = ON;"];
    [self.dbManager executeQuery:@"INSERT  OR IGNORE INTO  EXPENSESTATUS(STATUSNAME, STATUSID) VALUES('Waiting',1)"];
    [self.dbManager executeQuery:@"INSERT  OR IGNORE INTO  EXPENSESTATUS(STATUSNAME, STATUSID) VALUES('Approve',2)"];
    [self.dbManager executeQuery:@"INSERT  OR IGNORE INTO  EXPENSESTATUS(STATUSNAME, STATUSID) VALUES('Reject',3)"];
}
- (IBAction)JoinInHisApp:(id)sender {
    
    if([ApDelegate reachable])
    {
        if([self validateName:self.txtFieldName.text])
        {
            if([self validateEmail:self.txtFieldEmail.text] && [self validateEmailWithString:self.txtFieldEmail.text])
            {
                if ([self ValidatePhoneNumber:self.txtFieldMobile.text]) {
                    [self.navigationItem setHidesBackButton:YES animated:YES];
                    [self SendUserDetails];
                }else
                {
                    [self ValidationLabelAnimation:@"Mobile number is not valid"];
                }
            }else
            {
                [self ValidationLabelAnimation:@"Email address is not valid"];
            }
        }else
        {
            [self ValidationLabelAnimation:@"Name cannot be included with special characters"];
        }
    }
    else
    {
        [self AlertView:@"" strTitle:@"No Network Connection" isStatus:NO];
    }
}

-(void)ValidationLabelAnimation:(NSString*)ValidationMsg
{
    self.lblFieldValidation.text =ValidationMsg;
    self.lblFieldValidation.hidden = NO;
    self.lblFieldValidation.alpha = 0;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.lblFieldValidation.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5
                                               delay:5
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              self.lblFieldValidation.alpha = 0;
                                          }
                                          completion:nil];
                     }
     ];
}

-(void)SendUserDetails
{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
    [postDict setObject:self.txtFieldName.text forKey:@"name"];
    [postDict setObject:self.txtFieldEmail.text forKey:@"email"];
    [postDict setObject:[self.dbManager Stringvanish:self.txtFieldMobile.text] forKey:@"mobileno"];
    
    NSString *udid = [[NSUUID UUID] UUIDString];
    [postDict setObject:udid forKey:@"deviceid"];
    [postDict setObject:[UIDevice currentDevice].model forKey:@"brand"];
    
    NSLog(@"[UIDevice currentDevice].model: %@",[UIDevice currentDevice].model);
    NSLog(@"[UIDevice currentDevice].description: %@",[UIDevice currentDevice].description);
    NSLog(@"[UIDevice currentDevice].localizedModel: %@",[UIDevice currentDevice].localizedModel);
    NSLog(@"[UIDevice currentDevice].name: %@",[UIDevice currentDevice].name);
    NSLog(@"[UIDevice currentDevice].systemVersion: %@",[UIDevice currentDevice].systemVersion);
    NSLog(@"[UIDevice currentDevice].systemName: %@",[UIDevice currentDevice].systemName);
    
    
    RESTfulServices *LoginDetail_Rest = [[RESTfulServices alloc] init];
    
    [LoginDetail_Rest setDelegate:self];
    [LoginDetail_Rest setServiceIdentifier:@"SignIn"];
    [LoginDetail_Rest callServicePost:postDict forPath:urlRegistration];
    
    [ApDelegate ProgressView:self.view Messsage:@"Registering in HisApp"];
    [self KeyBoardDoneClicked:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"VerifyCode"])
    {
        
    }
}

- (IBAction)btnVerifyClick:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *MobileNo =[defaults objectForKey:@"ContactMobileNo"];
    
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc] init];
    [postDict setObject:MobileNo forKey:@"mobileno"];
    [postDict setObject:self.txtVerification.text forKey:@"key"];
    
    RESTfulServices *LoginDetail_Rest = [[RESTfulServices alloc] init];
    
    [LoginDetail_Rest setDelegate:self];
    [LoginDetail_Rest setServiceIdentifier:@"VerifyCode"];
    [LoginDetail_Rest callServicePost:postDict forPath:urlCodeActivation];
}


- (IBAction)btnPolicyClick:(id)sender {
    self.btnPolicy.titleLabel.attributedText = [self AttributeStringBold:2];

}
@end
