//
//  EventUserTableViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 26/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventUserTableViewController : UITableViewController
- (IBAction)Home_CloseClick:(id)sender;

@property(nonatomic,strong) NSMutableArray *MembersList;

@property(nonatomic,strong) NSString  *StrEventName;
@property(nonatomic,strong) NSString *StrEventId;

@end
