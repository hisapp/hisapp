//
//  GlobalVar.h
//  Game
//
//  Created by GAYATHIRIDEVI on 10/09/14.
//  Copyright (c) 2014 GAYATHIRIDEVI. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalVar : NSObject

@property (assign, nonatomic) NSInteger                     SelectedId;
@property (nonatomic,retain) NSMutableArray                 *arrOfTotalTripName;


@property (nonatomic,retain) NSMutableDictionary            *dictAllEvent;
@property (nonatomic,retain) NSMutableDictionary            *dictAllGroups;


@property (nonatomic, assign) BOOL                      dropDownStatus;


@property(nonatomic , retain) NSString                      *ProfileId;


+ (GlobalVar*)sharedObject;

+(void)PrintJson:(NSMutableArray *)pArr;
-(NSString *)Stringvanish:(NSString *)incorrect;
@end


//Class diagram link
//https://creately.com/app/?diagID=i4xxstzd1#