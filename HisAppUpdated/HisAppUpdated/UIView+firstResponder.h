
//
//  UIView+firstResponder.h
//  Hisaab
//
//  Created by GAYATHIRIDEVI on 27/02/15.
//  Copyright (c) 2015 GAYATHIRIDEVI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (firstResponder)

- (UIView *)findFirstResponder;

@end
