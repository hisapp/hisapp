//
//  PhoneBookObject.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 24/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "PhoneBookObject.h"

@implementation PhoneBookObject

@synthesize error,addressBook;

-(BOOL)CheckPhoneBookStatus
{
    error = nil;
    addressBook = ABAddressBookCreateWithOptions(NULL, error);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted) {
                self.status= granted;
            } else {
                self.status =granted;
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized) {
        self.status= YES;
    }
    else {}
    return self.status;
}

#pragma mark - Get Contact Details
-(NSMutableArray *)MArrayContactFunc
{
    self.ContactList = [[NSMutableArray alloc] init];
    //    CFErrorRef *error = nil;
    //
    //    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, error);
    ABRecordRef source = ABAddressBookCopyDefaultSource(addressBook);
    CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(addressBook, source, kABPersonSortByFirstName);
    CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
    self.ContactList = [NSMutableArray arrayWithCapacity:nPeople];
    
    
    for (int i = 0; i < nPeople; i++)
    {
        ContactDetail *contacts = [ContactDetail new];
        contacts.Isopen = NO;
        contacts.IsChecked = NO;
        ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
        
        //get First Name and Last Name
        
        contacts.firstName = (__bridge NSString*)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        
        contacts.lastName =  (__bridge NSString*)ABRecordCopyValue(person, kABPersonLastNameProperty);
        
        if (!contacts.firstName) {
            contacts.firstName = @"";
        }
        if (!contacts.lastName) {
            contacts.lastName = @"";
        }
        contacts.fullName = [contacts.firstName stringByAppendingString:[NSString stringWithFormat:@" %@" ,contacts.lastName]];
        //                NSLog(@"firstName  lastName  %@",contacts.fullName );
        
        // get contacts picture, if pic doesn't exists, show standart one
        
        //            NSData  *imgData = (__bridge NSData *)ABPersonCopyImageData(person);
        //            contacts.image = [UIImage imageWithData:imgData];
        //            if (!contacts.image) {
        //                contacts.image = [UIImage imageNamed:@"NOIMG.png"];
        //            }
        //get Phone Numbers
        
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        
        ABMultiValueRef multiPhones = ABRecordCopyValue(person, kABPersonPhoneProperty);
        for(CFIndex i=0;i<ABMultiValueGetCount(multiPhones);i++) {
            
            CFStringRef phoneNumberRef = ABMultiValueCopyValueAtIndex(multiPhones, i);
            NSString *phoneNumber = (__bridge NSString *) phoneNumberRef;
            [phoneNumbers addObject:[contacts Stringvanish:phoneNumber]];
            
            if(phoneNumberRef) CFRelease(phoneNumberRef);
        }
        //            NSLog(@"All numbers %@", phoneNumbers);
        [contacts setMobileNumbers:phoneNumbers];
        if([phoneNumbers count]>0)
        {
            [contacts setMobileNumber:[phoneNumbers objectAtIndex:0]];
        }
        CFRelease(multiPhones);
        //get Contact email
        
        NSMutableArray *contactEmails = [[NSMutableArray alloc] init];
        ABMultiValueRef multiEmails = ABRecordCopyValue(person, kABPersonEmailProperty);
        
        for (CFIndex i=0; i<ABMultiValueGetCount(multiEmails); i++) {
            CFStringRef contactEmailRef = ABMultiValueCopyValueAtIndex(multiEmails, i);
            NSString *contactEmail = (__bridge NSString *)contactEmailRef;
            
            [contactEmails addObject:contactEmail];
        }
        
        //            contacts.Emails = contactEmails;
        [contacts setEmails:contactEmails];
        if([contactEmails count] >0)
            [contacts setEmail:[contactEmails objectAtIndex:0]];
        CFRelease(multiEmails);
        
        [self.ContactList addObject:contacts];
        CFRelease(person);
    }
    
    CFRelease(allPeople);
    CFRelease(source);
//    CFRelease(addressBook);
    
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray = [self.ContactList sortedArrayUsingDescriptors:descriptors];
    
    self.ContactList = (NSMutableArray *)sortedArray;
    
    return self.ContactList;
}
@end
