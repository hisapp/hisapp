//

#import "SectionView.h"
#import <QuartzCore/QuartzCore.h>


@implementation SectionView

@synthesize section;
@synthesize delegate;

@synthesize lblName,ArrowButton;

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

- (void)awakeFromNib {
    // Initialization code
    
    
    // set the selected image for the disclosure button
    [self.ArrowButton setImage:[UIImage imageNamed:@"Expand.png"] forState:UIControlStateSelected];
    
    // set up the tap gesture recognizer
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(ArrowButtonClick:)];
    [self addGestureRecognizer:tapGesture];
    
    
//    if (self.CheckBoxStatus.selected)
//    {
        [self.CheckBoxStatus setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
//    } else
//    {
//        [self.CheckBoxStatus setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
//
//    }
}

- (IBAction)ArrowButtonClick:(id)sender {
    [self toggleButtonPressed:TRUE];
}

- (IBAction)CheckBoxClick:(id)sender {
    self.CheckBoxStatus.selected = !self.CheckBoxStatus.selected;
    
    UIButton *bb = (UIButton*)sender;
    
    if(!self.CheckBoxStatus.selected)
    {
        NSLog(@"CheckBoxClick tick %ld" , (long)bb.tag);

        if ([self.delegate respondsToSelector:@selector(TickCheckbox:)])
        {
            [self.delegate TickCheckbox:self.section];
        }
    }
    else
    {
        NSLog(@"CheckBoxClick UNtick ");

        if ([self.delegate respondsToSelector:@selector(UnTickCheckbox:)])
        {
            [self.delegate UnTickCheckbox:self.section];
        }
    }
}
- (void) toggleButtonPressed : (BOOL) flag
{
    self.ArrowButton.selected = !self.ArrowButton.selected;
    if(flag)
    {
        if (self.ArrowButton.selected)
        {
            if ([self.delegate respondsToSelector:@selector(sectionOpened:)])
            {
                [self.delegate sectionOpened:self.section];
            }
        } else
        {
            if ([self.delegate respondsToSelector:@selector(sectionClosed:)])
            {
                [self.delegate sectionClosed:self.section];
            }
        }
    }
}


@end
