//
//  MoneyEntryTableViewCell.h
//  Restapp
//
//  Created by GAYATHIRIDEVI on 08/01/15.
//  Copyright (c) 2015 GAYATHIRIDEVI. All rights reserved.
//

#import <UIKit/UIKit.h>

    @interface MoneyEntryTableViewCell : UITableViewCell


    @property (strong, nonatomic) IBOutlet UILabel *lblMemName;


    //Money Entry Cell

    @property (strong, nonatomic) IBOutlet UITextField *textAmount;

    @property (strong, nonatomic) IBOutlet UILabel *lblmemId;






    //Result View Cell
    @property (strong, nonatomic) IBOutlet UILabel *lblMemberTitle;
    @property (strong, nonatomic) IBOutlet UILabel *lblAmountPayable;

    @end
