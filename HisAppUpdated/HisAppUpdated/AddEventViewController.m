//
//  AddEventViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 23/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "AddEventViewController.h"
#import "EventUserViewController.h"

@interface AddEventViewController ()
{
    NSInteger FieldTag;
    NSDate *startDate, *EndDate;
}
@end

@implementation CALayer (Additions)
- (void)setBorderColorFromUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}
@end

@implementation AddEventViewController

- (void)viewDidLoad {
    self.btnEndDate.alpha =0.6;
    self.btnEndDate.userInteractionEnabled = NO;
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
}

-(void)viewWillDisappear:(BOOL)animated
{
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
       if([segue.identifier isEqualToString:@"AddUserInEvent"]){
           EventUserViewController *Control = (EventUserViewController *) segue.destinationViewController;
           Control.navigationItem.title = self.txtEventName.text;
           Control.StartDate = self.txtStartDate.text;
           Control.EndDate = self.txtEndDate.text;
       }
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:Nil];
}
- (IBAction)ShowDate:(id)sender {
    if([self.txtEventName isFirstResponder])
    {
        [self.txtEventName resignFirstResponder];
    }
    
    UIButton *btn = (UIButton *)sender;
    FieldTag =btn.tag;
    if(FieldTag ==4)
        self.lblDatePickerTitle.text =@"Select Start Date";
    else
    {
        self.lblDatePickerTitle.text = @"Select End Date";
        
//        self.datePicker.date =[[ NSDate alloc ] initWithTimeIntervalSinceNow: (NSTimeInterval) 2 ];
        self.datePicker.minimumDate = [[ NSDate alloc ] initWithTimeIntervalSinceNow: (NSTimeInterval) 0 ];
    }
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.lblDatePickerTitle.alpha = 1.0;
                         self.datePicker.alpha =1.0;
                         self.btnCancel.alpha =1.0;
                         self.btnDone.alpha=1.0;
                     }
                     completion:^(BOOL finished){
                         self.lblDatePickerTitle.hidden = !self.lblDatePickerTitle.hidden;
                         self.datePicker.hidden = !self.datePicker.hidden;
                         self.btnCancel.hidden = !self.btnCancel.hidden;
                         self.btnDone.hidden = !self.btnDone.hidden;
                     }];
}

- (void)datePickerChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    NSLog(@"%@",strDate);
}
- (IBAction)btnDateDone:(id)sender {

    NSDate* dt = self.datePicker.date;
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger units = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit;
    NSDateComponents *components = [calendar components:units fromDate:dt];
    NSInteger year = [components year];
    NSInteger day = [components day];
    
    NSDateFormatter *weekDay = [[NSDateFormatter alloc] init] ;
    [weekDay setDateFormat:@"EEEE"];
    
    NSDateFormatter *calMonth = [[NSDateFormatter alloc] init] ;
    [calMonth setDateFormat:@"MM"];
    
    
    if(FieldTag == 4)
    {
        startDate = self.datePicker.date;
                self.txtStartDate.text =  [NSString stringWithFormat:@"%@, %li-%@-%li",[[weekDay stringFromDate:dt] substringToIndex:3], (long)day, [calMonth stringFromDate:dt], (long)year];
            NSLog(@"%@",self.txtStartDate.text);
        self.btnEndDate.alpha= 1.0;
        self.btnEndDate.userInteractionEnabled =YES;
        
    }

    else
    {
        EndDate =self.datePicker.date;
        NSDateComponents *components;
        NSInteger days;
        components = [[NSCalendar currentCalendar] components: NSDayCalendarUnit
                                                     fromDate: startDate toDate: EndDate options: 0];
        days = [components day];
        
        if(days <0)
        {
           self.lblNotification.text =@"End Date must be same date of Start Date or Exceed";
            [UIView animateWithDuration:0.2
                             animations:^{
                                 self.lblNotification.alpha = 1.0;
                             }
                             completion:^(BOOL finished){
                                 self.lblNotification.hidden  =NO;
                             }];
            
            [UIView animateWithDuration:6
                             animations:^{
                                 self.lblNotification.alpha = 0.0;
                             }
                             completion:^(BOOL finished){
                                 self.lblNotification.hidden  =YES;
                             }];
        }
        else
        {
        self.txtEndDate.text = [NSString stringWithFormat:@"%@, %li-%@-%li",[[weekDay stringFromDate:dt] substringToIndex:3], (long)day, [calMonth stringFromDate:dt], (long)year];
             NSLog(@"%@",self.txtEndDate.text);
        }
        NSLog(@"%ld",(long)days);
   }
    [self HidePickerView];
    
}
- (IBAction)btnDateClose:(id)sender {
    [self HidePickerView];
}

-(void)HidePickerView
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.lblDatePickerTitle.alpha = 0.0;
                         self.datePicker.alpha =0.0;
                         self.btnCancel.alpha =0.0;
                         self.btnDone.alpha=0.0;
                     }
                     completion:^(BOOL finished){
                         self.lblDatePickerTitle.hidden = YES;
                         self.datePicker.hidden = YES;
                         self.btnCancel.hidden = YES;
                         self.btnDone.hidden = YES;
                     }];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyDefault) {
        [self.view endEditing:YES];
    }
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    [self HidePickerView];
    return YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

    UITouch *touch = [[event allTouches] anyObject];
    if ([self.txtEventName isFirstResponder] && [touch view] != self.txtEventName)
    {
        [self.txtEventName resignFirstResponder];
    }

    [super touchesBegan:touches withEvent:event];
}
- (IBAction)btnNextClick:(id)sender {
    NSString *strNotify = @"";
    
    if([self.txtEventName.text isEqualToString:@""])
    {
        strNotify = @"Event Name should not empty.";
    }
    
    if([self.txtStartDate.text isEqualToString:@""])
    {
        if([strNotify isEqualToString:@""])
            strNotify = @"Start Date should not empty.";
        else
            strNotify = [strNotify stringByAppendingString:@"Start Date should not empty."];
    }
    
    if([self.txtEndDate.text isEqualToString:@""])
    {
        if([strNotify isEqualToString:@""])
            strNotify = @"End Date should not empty.";
        else
            strNotify = [strNotify stringByAppendingString:@"End Date should not empty."];
    }
    
    if([strNotify isEqualToString:@""])
    {
        [self performSegueWithIdentifier:@"AddUserInEvent" sender:self];
    }
    else
    {
        self.lblNotification.text = strNotify;
        [UIView animateWithDuration:0.2
                         animations:^{
                             self.lblNotification.alpha = 1.0;
                         }
                         completion:^(BOOL finished){
                             self.lblNotification.hidden  =NO;
                         }];
        
        [UIView animateWithDuration:5
                         animations:^{
                             self.lblNotification.alpha = 0.0;
                         }
                         completion:^(BOOL finished){
                             self.lblNotification.hidden  =YES;
                         }];
    }
}
@end

