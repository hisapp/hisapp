//
//  AddGroupViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 14/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "AddGroupViewController.h"
#import "ConatctViewController.h"
#import "DBManager.h"

#define IsEqual(x,y) ((x && [x isEqual:y]) || (!x && !y))
@interface AddGroupViewController ()
{
    NSString *query;
}
@property(nonatomic)   NSMutableArray *temp;
@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation AddGroupViewController
@synthesize arrMembersList;
- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    if(self.arrMembersList ==nil)
    {
        self.arrMembersList = [[NSMutableArray alloc] init];
    }
    
    if(self.StrNameGroup !=nil)
    {
        self.txtGroupName.text = self.StrNameGroup;
        [self.navigationItem.rightBarButtonItem setTitle:self.strBtnName];
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.TblVw_GroupMembers reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if(self.TblVw_GroupMembers.editing)
    {
        [super setEditing:NO animated:YES];
        [self.TblVw_GroupMembers setEditing:NO animated:YES];
        [self.navigationItem.leftBarButtonItem setTitle:@"Close"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
    }
}

#pragma mark - Navigation



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"ContactsFromAddGroup"]){
        ConatctViewController *controller = (ConatctViewController *)segue.destinationViewController;
        controller.delegate = self;
        controller.TempContact = self.arrMembersList;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([touch view] != self.txtGroupName)
    {
        [self.txtGroupName resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyDone) {
        [self.view endEditing:YES];
    }
    return YES;
}


-(void)addItemViewController:(ConatctViewController *)controller didFinishEnteringItem:(NSMutableArray *)SelectedContact
{
    NSMutableArray *myData = [[NSMutableArray alloc] init];
//    self.arrMembersList = [[NSMutableArray alloc] init];
    
    for (ContactDetail *cont in SelectedContact) {
        if(![self.arrMembersList containsObject:cont.fullName])
        {
            NSArray *temp = [NSArray arrayWithObjects:cont.fullName,cont.mobileNumber, nil];
            [self.arrMembersList addObject:temp];
        }
        [myData addObject:[cont dictionary]];
    }
    
    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:self.arrMembersList];
    self.arrMembersList = [[NSMutableArray alloc] initWithArray:[mySet array]];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:myData options:NSJSONWritingPrettyPrinted error:&error];
    if ([jsonData length] > 0 &&
        error == nil){
        //        NSLog(@"Successfully serialized the dictionary into data = %@", jsonData);
//        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        NSLog(@"JSON String = %@", jsonString);
    }
    else if ([jsonData length] == 0 &&
             error == nil){
        NSLog(@"No data was returned after serialization.");
    }
    else if (error != nil){
        NSLog(@"An error happened = %@", error);
    }
}

#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (NSInteger)[self.arrMembersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MemberListCellIdentifier =@"MemberListCell";
    
    UITableViewCell *cell = (UITableViewCell *)[self.TblVw_GroupMembers dequeueReusableCellWithIdentifier:MemberListCellIdentifier];
    
    if(cell ==nil)
    {}
    
    UILabel *Name = (UILabel *)[cell.contentView viewWithTag:1];
    Name.text = [[self.arrMembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    
    UILabel *hiddenId = (UILabel *)[cell.contentView viewWithTag:2];
    hiddenId.text =[[self.arrMembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UILabel *Cont_Name = (UILabel *)[cell.contentView viewWithTag:1];
        UILabel *Cont_Id = (UILabel *)[cell.contentView viewWithTag:2];
         NSLog(@"%@--------%@", Cont_Id.text, Cont_Name.text);
        
        [self.arrMembersList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        if([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Update"])
        {
            query =[NSString stringWithFormat:@"DELETE FROM USERGROUPREL  WHERE userid=%@  and groupid =%@ ",Cont_Id.text,self.StrGroupId] ;
            [self.dbManager executeQuery:query];
        }
    }
}

-(void)InsertMembersInGroups:(NSString *)G_Id
{
    for(NSArray *cc in self.arrMembersList)
    {
        query =[NSString stringWithFormat:@"Select * from USERGROUPREL  where USERID =%@ and GROUPID = %@",[cc objectAtIndex:1],G_Id];
        if(![self.dbManager recordExistOrNot:query])
        {
            query =[NSString stringWithFormat:@"insert into USERGROUPREL(USERID,GROUPID) values (%@,%@)",[cc objectAtIndex:1],G_Id];
            [self.dbManager executeQuery:query];
            if (self.dbManager.affectedRows != 0)
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            else
                NSLog(@"Could not execute the query.");
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Actions

- (IBAction)DoneClick:(id)sender {
    if([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Delete All"])
    {
        [self.arrMembersList removeAllObjects];
        [super setEditing:NO animated:YES];
        [self.TblVw_GroupMembers setEditing:NO animated:YES];
        [self.TblVw_GroupMembers reloadData];
        
        
        [self.navigationItem.leftBarButtonItem setTitle:@"Close"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
//        if([self.strBtnName isEqualToString:@""])
        if (IsEqual(self.strBtnName, nil))
        {
            [self.navigationItem.rightBarButtonItem setTitle:@"Done"];
        }
        else
        {
            [self.navigationItem.rightBarButtonItem setTitle:self.strBtnName];
        }
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
    else
    {
        if(![self.txtGroupName.text  isEqualToString:@""] && [self.arrMembersList count] >0)
        {
            
            if ([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Done"])
            {

                
                query =[NSString stringWithFormat:@"insert into groups(groupName,dateCreated,dateModified,archived) values('%@',current_timestamp,current_timestamp,'no')",self.txtGroupName.text];

                [self.dbManager executeQuery:query];
                
                NSArray *arrLastId = [self.dbManager loadDataFromDB:@"select seq from sqlite_sequence where name ='GROUPS'"];
                NSString *GroupId = [[arrLastId objectAtIndex:0] objectAtIndex:0];
                
                [self InsertMembersInGroups:GroupId];
            }
            else if ([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Update"])
            {
                query =[NSString stringWithFormat:@"UPDATE groups set dateModified = current_timestamp , GROUPNAME = '%@' where groupid = %@",self.txtGroupName.text,self.StrGroupId];
                
                [self.dbManager executeQuery:query];
                [self InsertMembersInGroups:self.StrGroupId];
            }
        }
        else
        {
            [self ShowAlertView:@"Group name should not be empty and Group has atleast one member."];
        }
    }
}

- (IBAction)Close:(id)sender {
    if([self.navigationItem.leftBarButtonItem.title isEqualToString:@"Close"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    if([self.navigationItem.leftBarButtonItem.title isEqualToString:@"Cancel"])
    {
        [super setEditing:NO animated:YES];
        [self.TblVw_GroupMembers setEditing:NO animated:YES];
        [self.navigationItem.leftBarButtonItem setTitle:@"Close"];
        [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
        
//         if([self.strBtnName isEqualToString:@""])
        if (IsEqual(self.strBtnName, nil))
            [self.navigationItem.rightBarButtonItem setTitle:@"Done"];
        else
            [self.navigationItem.rightBarButtonItem setTitle:self.strBtnName];
        
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
    }
}

-(void)ShowAlertView:(NSString *)alertVwString
{
    UIAlertView *alertview =[[UIAlertView alloc] initWithTitle:@"Warning" message:alertVwString delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alertview.tag =2;
    [alertview  show];
}
- (IBAction)AddHisAppUser:(id)sender {
}

- (IBAction)AddFromContacts:(id)sender {
    
    [self performSegueWithIdentifier:@"ContactsFromAddGroup" sender:self];
}

- (IBAction)EditUser:(id)sender {
    
    if([self.arrMembersList count] >0)
    {
        if(self.TblVw_GroupMembers.editing)
        {
            [super setEditing:NO animated:YES];
            [self.TblVw_GroupMembers setEditing:NO animated:YES];
            [self.navigationItem.leftBarButtonItem setTitle:@"Close"];
            [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
            
//            if([self.strBtnName isEqualToString:@""])
             if (IsEqual(self.strBtnName, nil))
                [self.navigationItem.rightBarButtonItem setTitle:@"Done"];
            else
                [self.navigationItem.rightBarButtonItem setTitle:self.strBtnName];

            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0]];
            
        }
        else
        {
            [super setEditing:YES animated:YES];
            [self.TblVw_GroupMembers setEditing:YES animated:YES];
            [self.navigationItem.leftBarButtonItem setTitle:@"Cancel"];
            [self.navigationItem.leftBarButtonItem setStyle:UIBarButtonItemStylePlain];
            
            [self.navigationItem.rightBarButtonItem setTitle:@"Delete All"];
            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
            [self.navigationItem.rightBarButtonItem setTintColor:[UIColor redColor]];
            
        }
    }
}
@end
