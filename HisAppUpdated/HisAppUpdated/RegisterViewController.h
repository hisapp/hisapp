//
//  RegisterViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 09/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "RESTfulServices.h"

#import "UIView+firstResponder.h"
#import "Reachability.h"

#import "Constants.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,RestfulDelegate>

- (IBAction)Register:(id)sender;
- (IBAction)JoinInHisApp:(id)sender;
- (IBAction)btnPolicyClick:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtFieldName;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollViewContainer;
@property (strong, nonatomic) IBOutlet UIButton *JoinInHisapp;
@property (strong, nonatomic) IBOutlet UINavigationItem *navBarTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnJoinInHisApp;

@property (strong, nonatomic) IBOutlet UILabel *lblFieldValidation;

@property (strong, nonatomic) IBOutlet UIButton *btnPolicy;


//Global DbManager
@property (nonatomic, strong) DBManager *dbManager;



@property (strong, nonatomic) IBOutlet UITextField *txtVerification;
- (IBAction)btnVerifyClick:(id)sender;
@end
