//
//  RESTfulServices.m
//  SPFFoods
//
//  Created by DURGA PRASAD on 22/02/14.
//  Copyright (c) 2014 SPFSolutions, LLC. All rights reserved.
//

#import "RESTfulServices.h"

@implementation RESTfulServices
@synthesize serviceIdentifier,responseData;

-(void)setDelegate:(id)_delegate
{
    delegate = _delegate;
}

- (void)callServiceGet:(NSString*)method forPath:(NSString*)serverpath
{
    serverpath	= [serverpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:serverpath]];
    [request setHTTPMethod:method];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    connection  =    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (!connection)
    {
        request = nil;
        [delegate RestServiceFailedwithError:nil forIdentifier:serviceIdentifier];
        return;
    }
    [connection start];
}

- (void)callServicePost:(NSMutableDictionary*)dict forPath:(NSString*)strPath
{
    strPath	= [strPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request= [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:strPath]];
    [request setHTTPMethod:@"POST"];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        
        jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
        
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

        NSLog(@"Json Details------ %@", jsonString);
    }
    
    [request setHTTPBody:jsonData];
    
    connection  =    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [connection start];
}


#pragma mark -

#pragma mark - NSURL Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    responseData = [NSMutableData data];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [responseData appendData:data];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSMutableDictionary *finalDictResult = [[NSMutableDictionary alloc] init];
    
    NSDictionary *dictResult =  [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
    if([dictResult isKindOfClass:[NSDictionary class]]){
        NSLog(@"It is Dictionary");
        finalDictResult = [dictResult mutableCopy];
    }
    if([dictResult isKindOfClass:[NSArray class]]) {
        NSLog(@"It is Array");
        [finalDictResult setObject:dictResult forKey:@"zipcode"];
    }
    if ([[finalDictResult allKeys] containsObject:@"errors"]) {
        [delegate RestServiceFailedwithError:[finalDictResult objectForKey:@"errors"] forIdentifier:serviceIdentifier];
    }
    else{
        [delegate RestServiceLoadedData:responseData forIdentifier:serviceIdentifier];
    }
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [delegate RestServiceFailedwithError:error forIdentifier:serviceIdentifier];
}
#pragma mark -





@end
