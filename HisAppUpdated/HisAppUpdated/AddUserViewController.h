//
//  AddUserViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 06/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>




@interface AddUserViewController : UIViewController
- (IBAction)AddUserClick:(id)sender;
- (IBAction)ClearClick:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtMail;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;

@end
