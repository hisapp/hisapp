//
//  AddExpenseViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "AddExpenseViewController.h"
#import "UIView+firstResponder.h"
#import "SplitExpenseViewController.h"
#import "DBManager.h"


#import "DemoTableController.h"


#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"

@interface AddExpenseViewController ()
{
    
    NSMutableDictionary *dictUserInExpense;
}

@property(nonatomic) UIBarButtonItem* PrevButton;
@property(nonatomic) UIBarButtonItem*  NextButton;
@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation AddExpenseViewController

- (void)viewDidLoad {
    
    dictUserInExpense = [[NSMutableDictionary alloc] init];
    
    self.dbManager = [DBManager sharedObject];
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    
    self.PrevButton = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:105 target:self  action:@selector(Previous:)];
    self.NextButton= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:106  target:self action:@selector(Next:)];
    
    UIBarButtonItem* fake = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil] ;
    UIBarButtonItem* flexSpace = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects: self.PrevButton,fake,self.NextButton,fake,flexSpace,fake,doneButton,nil] animated:YES];
    
    self.txtExpenseName.inputAccessoryView = keyboardDoneButtonView;    self.txtAmount.inputAccessoryView = keyboardDoneButtonView;
    
    
    
    [self.ImgVwTick setImage:[UIImage imageNamed:@""]];
    [super viewDidLoad];





}


-(void)viewWillAppear:(BOOL)animated
{
    [self LoadDbData];
    [self.tblVwListUser reloadData];
    // [self.txtExpenseName becomeFirstResponder];
}



-(void)LoadDbData
{
    if(self.MembersList ==nil)
    {
        self.MembersList = [[NSMutableArray alloc] init];
    }
    NSString *query = [NSString stringWithFormat:@"Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USEREVENTREL where EVENTID =%@) ORDER BY CONTACTNAME ASC",self.StrEventId];
    self.MembersList = (NSMutableArray *)[self.dbManager loadDataFromDB:query];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - keyboard Accessory View

- (void)Previous:(id)sender
{
    [self.NextButton setEnabled:YES];
    
    UIView *responder = [self.view findFirstResponder];
    
    if (nil == responder || ![responder isKindOfClass:[UITextField class]]) {
        return;
    }
    if(responder.tag ==2)
    {
        [self.txtExpenseName becomeFirstResponder];
        [self.PrevButton setEnabled:NO];
    }
}

- (void)Next:(id)sender
{
    [self.PrevButton setEnabled:YES];
    UIView *responder = [self.view findFirstResponder];
    
    if (nil == responder || ![responder isKindOfClass:[UITextField class]]) {
        return;
    }
    if (responder.tag ==1)
    {
        [self.txtAmount becomeFirstResponder];
        [self.NextButton setEnabled:NO];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.PrevButton setEnabled:YES];
    [self.NextButton setEnabled:YES];
    if(textField.tag ==1)
        [self.PrevButton setEnabled:NO];
    else if (textField.tag ==2)
        [self.NextButton setEnabled:NO];
    
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType==UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    }
    else if (textField.returnKeyType == UIReturnKeyDone)
    {
        [self.view endEditing:YES];
    }
    return YES;
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"UserAmountDetail"])
    {
        NSMutableArray *arrSelectedUser = [[NSMutableArray alloc] init];
        NSArray *allKey = [dictUserInExpense allKeys];
        
        for(NSString *dd in allKey)
        {
            NSMutableArray *arrUser = [[NSMutableArray alloc] init];
            [arrUser addObject:[dictUserInExpense valueForKeyPath:[NSString stringWithFormat:@"%@.Name" , dd]]];
            [arrUser addObject:[dictUserInExpense valueForKeyPath:[NSString stringWithFormat:@"%@.Id" , dd]]];
            
            [arrSelectedUser addObject:arrUser];
        }
        
        SplitExpenseViewController *SEVC = segue.destinationViewController;
        SEVC.navigationItem.title = self.txtExpenseName.text;
        [SEVC setStrConfirmAmount:self.txtAmount.text];
        SEVC.StrEventId = self.StrEventId;
        SEVC.PaidbyDetail = self.PaidByDetail;
        SEVC.MembersList = arrSelectedUser;
    }
}

- (IBAction)CloseClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)NextClick:(id)sender {
    [self performSegueWithIdentifier:@"UserAmountDetail" sender:self];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender selectedArr:(NSArray *)Sarr{
    self.PaidByDetail = Sarr;
    [self rel];
}
-(void)rel{
    //    [dropDown release];
    dropDown = nil;
    [self LoadDbData];
    [self.tblVwListUser reloadData];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [[event allTouches] anyObject];
    if ( [touch view] != dropDown)
    {
          [self rel];
    }
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)PaidByPopClick:(id)sender {
    
    
    
   [self.view endEditing:YES];
    
    NSArray * arr = [[NSArray alloc] init];
    
    NSString *query = [NSString stringWithFormat:@"Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USEREVENTREL where EVENTID =%@) ORDER BY CONTACTNAME ASC",self.StrEventId];
    
    arr = [self.dbManager  loadDataFromDB:query];
    
    NSArray * arrImage = [[NSArray alloc] init];
    arrImage = [NSArray arrayWithObjects:[UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], [UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], [UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], [UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], [UIImage imageNamed:@"apple.png"], [UIImage imageNamed:@"apple2.png"], nil];
    
    
    if(dropDown == nil) {
        
        CGFloat h = [arr count] * 38;
        CGFloat height = self.view.bounds.size.height - self.PaiByPop.frame.origin.y - self.PaiByPop.bounds.size.height -10.0;
        
        
        CGFloat f =0;
        if(h > height)
        {
            f = height;
        }
        else
        {
            f = h;
        }
        
        dropDown = [[NIDropDown alloc] showDropDown:sender height:&f array:arr imgArray:arrImage direc:@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
    
    
    //[self popover:sender];
    
    
    //    DemoTableController *controller = [[DemoTableController    alloc] initWithStyle:UITableViewStylePlain];
    //
    //    //our popover
    //    popover = [[FPPopoverController alloc] initWithViewController:controller];
    //
    //    //the popover will be presented from the okButton view
    //    [popover presentPopoverFromView:sender];
}



-(void)selectedTableRow:(NSArray *)Paidby
{
    NSLog(@"SELECTED ROW %@",[Paidby objectAtIndex:0]);
    [self.PaiByPop setTitle:[Paidby objectAtIndex:0] forState:UIControlStateNormal];
    self.PaidByDetail = Paidby;
}
- (IBAction)btnTickClick:(id)sender {
    [self SelectAllUser];
}

- (IBAction)btnSelectAll:(id)sender {
    [self SelectAllUser];
}


-(void)SelectAllUser
{
    self.btnTck.selected =!self.btnTck.selected;
    
    if(self.btnTck.selected)
    {
        for(NSArray *arr in self.MembersList)
        {
            if([dictUserInExpense objectForKey:[self.MembersList objectAtIndex:1]] == nil)
            {
                NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                [dict setObject:[arr objectAtIndex:1] forKey:@"Id"];
                [dict setObject:[arr objectAtIndex:0]  forKey:@"Name"];
                [dictUserInExpense setObject:dict forKey:[arr objectAtIndex:1]];
            }
        }
        [self.ImgVwTick setImage:[UIImage imageNamed:@"Checkmark.png"]];
    }
    else
    {
        [dictUserInExpense removeAllObjects];
        [self.ImgVwTick setImage:[UIImage imageNamed:@""]];
    }
    
    [self.tblVwListUser reloadData];
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.MembersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"UserList"];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if (!cell)
    {
        
    }
    UILabel *lblID = (UILabel *) [cell.contentView viewWithTag:9];
    lblID.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    
    UILabel *lblName = (UILabel *) [cell.contentView viewWithTag:8];
    lblName.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    
    if([dictUserInExpense objectForKey:lblID.text] != nil)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UILabel *lblID = (UILabel *) [cell.contentView viewWithTag:9];
    UILabel *lblName = (UILabel *) [cell.contentView viewWithTag:8];
    
    
    if([dictUserInExpense objectForKey:lblID.text] ==nil)
    {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:lblID.text forKey:@"Id"];
        [dict setObject:lblName.text forKey:@"Name"];
        
        [dictUserInExpense setObject:dict forKey:lblID.text];
    }
    else
    {
        
        if(self.btnTck.selected)
        {
            self.btnTck.selected =!self.btnTck.selected;
            [self.ImgVwTick setImage:[UIImage imageNamed:@""]];
        }
        
        [dictUserInExpense removeObjectForKey:lblID.text];
    }
    
    [self.tblVwListUser reloadData];
}
@end
