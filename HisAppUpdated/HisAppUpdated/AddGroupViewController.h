//
//  AddGroupViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 14/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConatctViewController.h"
#import "ContactDetail.h"
#import "AddUserViewController.h"




//@class AddGroupViewController;
//
//
//@protocol SendAddGroupDetail <NSObject>
//- (void)GetAddGroupDetail:(AddGroupViewController *)controller didFinishEnteringItem:(NSMutableArray *)SelectedContact;
//@end

@interface AddGroupViewController : UIViewController<SendContactDetail>

- (IBAction)Close:(id)sender;
- (IBAction)DoneClick:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *TblVw_GroupMembers;
@property (strong, nonatomic) IBOutlet UITextField *txtGroupName;

@property(nonatomic) ContactDetail *contacts;
@property(nonatomic,strong) NSMutableArray *arrMembersList;

@property(nonatomic,strong) NSString  *StrNameGroup;
@property(nonatomic,strong) NSString *StrGroupId;
@property(nonatomic , strong) NSString *strBtnName;


- (IBAction)AddHisAppUser:(id)sender;
- (IBAction)AddFromContacts:(id)sender;
- (IBAction)EditUser:(id)sender;

//@property (weak) id <SendAddGroupDetail> delegate;


@end
