
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol SectionView;

@interface SectionView : UITableViewHeaderFooterView
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *ArrowButton;
- (IBAction)ArrowButtonClick:(id)sender;
- (IBAction)CheckBoxClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *CheckBoxStatus;

@property (nonatomic) NSInteger section;
@property (weak) id<SectionView> delegate;

- (void) toggleButtonPressed : (BOOL) flag;

@end

@protocol SectionView <NSObject>

@optional
- (void) sectionClosed : (NSInteger) section;
- (void) sectionOpened : (NSInteger) section;


- (void) TickCheckbox : (NSInteger) section;
- (void) UnTickCheckbox : (NSInteger) section;
@end