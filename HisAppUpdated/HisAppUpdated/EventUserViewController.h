//
//  EventUserViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 24/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhoneBookObject.h"


@interface EventUserViewController : UIViewController

@property(nonatomic) PhoneBookObject *PhoneObject;
@property(nonatomic) ContactDetail *contacts;

@property (nonatomic) NSMutableArray *memberList;
@property(nonatomic) NSMutableArray *SelectedList;

@property (strong, nonatomic) IBOutlet UITableView *tblVwMeberlist;
- (IBAction)Segment_Click:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
- (IBAction)DoneClick:(id)sender;


@property (nonatomic) NSString *StartDate, *EndDate;

@end
