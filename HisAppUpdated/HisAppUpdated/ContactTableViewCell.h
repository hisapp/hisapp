//
//  ContactTableViewCell.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 14/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ContactTableViewCell;

@protocol MyCustomCellDelegate;

@protocol MyCustomCellDelegate <NSObject>
@optional
- (void)buttonTappedOnCell:(ContactTableViewCell *)cell;

//-(void)resignKeyboardfromCell:(ContactTableViewCell *)cell;

@end



@interface ContactTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCeckBox;
- (IBAction)btnCheckBoxClick:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblName;

- (IBAction)BtnArrowClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnArrow;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwBox;
@property (strong, nonatomic) IBOutlet UIImageView *imgVwTick;


@property (weak) id<MyCustomCellDelegate> Customdelegate;

- (void) setDelegate:(id)delegate;

@end
