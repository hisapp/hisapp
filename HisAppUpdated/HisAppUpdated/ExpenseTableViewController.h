//
//  ExpenseTableViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 28/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExpenseTableViewController : UITableViewController
- (IBAction)Home_CloseClick:(id)sender;
@property(nonatomic,strong) NSString *StrEventId;
- (IBAction)AddExpenseClick:(id)sender;

//AddExpense

@property(nonatomic, strong) NSMutableArray *ExpenseList, *ExpenseDetail ;

@end
