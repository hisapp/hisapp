//
//  EventUserViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 24/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "EventUserViewController.h"
#import "DBManager.h"


@interface EventUserViewController ()
{
    NSString *query;
}
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation EventUserViewController

- (void)viewDidLoad {
    

    self.dbManager = [DBManager sharedObject];
    self.PhoneObject = [[PhoneBookObject alloc]init];
    self.SelectedList = [[NSMutableArray alloc] init];
    
    [self GetHisappContacts];
    [self AscendingList];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.memberList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.contacts = [self.memberList objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        NSLog(@"New Cell Cretaed");
    }
    
    UILabel *lblName = (UILabel *) [cell.contentView viewWithTag:1];
    lblName.text = self.contacts.fullName;
    cell.accessoryType = UITableViewCellAccessoryNone;

    
    for(NSString *t in self.SelectedList)
    {
        if([t isEqualToString:self.contacts.mobileNumber]) //    if([self.SelectedList containsObject:self.contacts])
        {
            if(self.contacts.IsChecked)
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            else
                cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [self.tblVwMeberlist cellForRowAtIndexPath:indexPath];
    self.contacts = [self.memberList objectAtIndex:indexPath.row];
    self.contacts.IsChecked = ! self.contacts.IsChecked;
    
    if(self.contacts.IsChecked)
        {
            if ([self.SelectedList containsObject:self.contacts.mobileNumber])
            {}
            else
                [self.SelectedList addObject:self.contacts.mobileNumber];
        }
        else
            [self.SelectedList removeObject:self.contacts.mobileNumber];
    [self.tblVwMeberlist reloadData];
}



-(void)ShowAlert
{
    UIAlertView *contact = [[UIAlertView alloc] initWithTitle:@"Access error" message:@"HisApp cannot access your Contacts. Kindly check in Settings -> Privacy -> Contacts." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [contact show];
}
- (IBAction)Segment_Click:(id)sender {
    
    
    switch (self.segmentControl.selectedSegmentIndex)
    {
        case 0:
        {
            [self GetHisappContacts];
            break;
        }
        case 1:
        {
            [self GetContactDetail];
            break;
        }
        case 2:
        {
            
            [self GetGroupDetails];
            break;
        }
        default:
            break;
    }
    
    [self AscendingList];
    [self AnimateTable];
}

#pragma mark - Done Click Function 

- (IBAction)DoneClick:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ProfileId =[defaults objectForKey:@"ContactMobileNo"];
    
    query =[NSString stringWithFormat:@"insert into EVENTS(eventName,dateCreated,startDate,endDate,eventAdmin) values ('%@',current_timestamp,'%@','%@',%@)",self.navigationItem.title, self.StartDate, self.EndDate,ProfileId];
    
    [self.dbManager executeQuery:query];
    if (self.dbManager.affectedRows != 0)
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    else
        NSLog(@"Could not execute the query.");
    
    NSArray *arrLastId = [self.dbManager loadDataFromDB:@"select seq from sqlite_sequence where name='EVENTS'"];
    NSString * lastId = [[arrLastId objectAtIndex:0] objectAtIndex:0];
    
    query = [NSString stringWithFormat:@"insert into USEREVENTREL(USERID,EVENTID) values(%@,%@)",ProfileId,lastId];
    [self.dbManager executeQuery:query];
    
    [self  getMemberListForEvents];
    
//    NSLog(@"===================%@",lastId);

//    NSLog(@"%@-------%@-------- %@----", self.navigationItem.title, self.StartDate, self.EndDate);
    
//    for(ContactDetail *cc in self.memberList)
//    {
//        if(cc.IsChecked)
//        {
//            query = [NSString stringWithFormat:@"Select * from Contacts where CONTACTID=%@ ",cc.mobileNumber];
//            
//            if(![self.dbManager recordExistOrNot:query])
//            {
//                NSArray *aa = [NSArray arrayWithArray:cc.mobileNumbers];
//                NSString *phNos = [aa componentsJoinedByString:@"|"];
//                query =[NSString stringWithFormat:@"insert into contacts(contactID,contactName,contactNo,contactEmail,archived )values(%@,'%@','%@','%@','no')",cc.mobileNumber,cc.fullName,phNos,cc.Email];
//                [self.dbManager executeQuery:query];
//            }
//            query = [NSString stringWithFormat:@"insert into USEREVENTREL(USERID,EVENTID) values(%@,%@)",cc.mobileNumber,lastId];
//            
//            [self.dbManager executeQuery:query];
//
//        }
//    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getMemberListForEvents
{
    [self GetHisappContacts];
    [self AssignMemberFromList];
    
    [self GetContactDetail];
    [self AssignMemberFromList];
    
    [self GetGroupDetails];
    [self AssignMemberFromGroup];
}

-(void)AssignMemberFromList
{
    NSArray *arrLastId = [self.dbManager loadDataFromDB:@"select seq from sqlite_sequence where name='EVENTS'"];
    NSString * lastId = [[arrLastId objectAtIndex:0] objectAtIndex:0];

    for(ContactDetail *cc in self.memberList)
    {
        for(NSString *str in self.SelectedList)
        {
            if([cc.mobileNumber isEqualToString:str])
            {
                query = [NSString stringWithFormat:@"Select * from Contacts where CONTACTID=%@ ",cc.mobileNumber];
                if(![self.dbManager recordExistOrNot:query])
                {
                    NSArray *aa = [NSArray arrayWithArray:cc.mobileNumbers];
                    NSString *phNos = [aa componentsJoinedByString:@"|"];
                    query =[NSString stringWithFormat:@"insert into contacts(contactID,contactName,contactNo,contactEmail,archived )values(%@,'%@','%@','%@','no')",cc.mobileNumber,cc.fullName,phNos,cc.Email];
                    [self.dbManager executeQuery:query];
                }
                query = [NSString stringWithFormat:@"insert into USEREVENTREL(USERID,EVENTID) values(%@,%@)",cc.mobileNumber,lastId];
                [self.dbManager executeQuery:query];
            }
        }
    }
}


-(void)AssignMemberFromGroup
{

    //    Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USERGROUPREL where groupid in (select groupid from  groups where groupid = 1))
    NSArray *arrLastId = [self.dbManager loadDataFromDB:@"select seq from sqlite_sequence where name='EVENTS'"];
    NSString * lastId = [[arrLastId objectAtIndex:0] objectAtIndex:0];
    for(ContactDetail *cc in self.memberList)
    {
        for(NSString *str in self.SelectedList)
        {
            if([cc.mobileNumber isEqualToString:str])
            {
                query = [NSString stringWithFormat:@"Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USERGROUPREL where groupid in (select groupid from  groups where groupid = %@)) ",cc.mobileNumber];
                NSArray *tt = [self.dbManager loadDataFromDB:query];
                for (NSArray *aa in [tt copy])
                {
                     query = [NSString stringWithFormat:@"Select * from USEREVENTREL where EVENTID =%@ and USERID =%@",lastId, [aa objectAtIndex:1]];
                    if(![self.dbManager recordExistOrNot:query])
                    {
                        query = [NSString stringWithFormat:@"insert into USEREVENTREL(USERID,EVENTID) values(%@,%@)",[aa objectAtIndex:1],lastId];
                        [self.dbManager executeQuery:query];
                    }
                }
            }
        }
    }
    
}

#pragma mark - Segment Change Function



-(void)AnimateTable
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.tblVwMeberlist.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self.tblVwMeberlist.alpha = 0.5;
                     }];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.tblVwMeberlist.alpha = 0.5;
                     }
                     completion:^(BOOL finished){
                         self.tblVwMeberlist.alpha = 1.0;
                     }];
    [self.tblVwMeberlist reloadData];
}

-(void)GetContactDetail

{
    self.memberList = [[NSMutableArray alloc] init];
    if([self.PhoneObject CheckPhoneBookStatus])
        self.memberList = [self.PhoneObject MArrayContactFunc];
    else
        [self ShowAlert];
    
    for(ContactDetail *new in self.memberList) {
        for (NSString *sel in self.SelectedList){
            if([sel isEqualToString:new.mobileNumber])
            {
                new.IsChecked = YES ;
            }
        }
    }
}

-(void)GetHisappContacts
{
    self.memberList = [[NSMutableArray alloc] init];
    query = @"Select * from Contacts where archived = 'no'";
    NSArray *temp = [self.dbManager loadDataFromDB:query];
    for(NSArray *his in temp)
    {
        self.contacts = [[ContactDetail alloc] init];
        
        self.contacts.mobileNumber = [his objectAtIndex:0];
        self.contacts.fullName = [his objectAtIndex:1];
        NSArray *aa = [[his objectAtIndex:2] componentsSeparatedByString:@"|"];
        self.contacts.mobileNumbers = (NSMutableArray *) aa;
        self.contacts.IsChecked = NO;
        self.contacts.Isopen = NO;
        [self.memberList addObject:self.contacts];
    }
    for(ContactDetail *new in self.memberList) {
        for (NSString *sel in self.SelectedList){
            if([sel isEqualToString:new.mobileNumber])
            {
                new.IsChecked = YES ;
            }
        }
    }
}

-(void)GetGroupDetails
{
    self.memberList = [[NSMutableArray alloc] init];
    query = @"Select * from Groups where archived = 'no'";
    NSArray *temp = [self.dbManager loadDataFromDB:query];
    for(NSArray *his in temp)
    {
        self.contacts = [[ContactDetail alloc] init];
        
        self.contacts.mobileNumber = [his objectAtIndex:0];
        self.contacts.fullName = [his objectAtIndex:1];
        self.contacts.IsChecked = NO;
        self.contacts.Isopen = YES;
        [self.memberList addObject:self.contacts];
    }
    
    for(ContactDetail *new in self.memberList) {
        for (NSString *sel in self.SelectedList){
            if([sel isEqualToString:new.mobileNumber])
            {
                new.IsChecked = YES ;
            }
        }
    }
}

-(void)AscendingList
{
    NSSortDescriptor *valueDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullName" ascending:YES];
    NSArray *descriptors = [NSArray arrayWithObject:valueDescriptor];
    NSArray *sortedArray = [self.memberList sortedArrayUsingDescriptors:descriptors];
    
    self.memberList = (NSMutableArray *)sortedArray;
}



@end