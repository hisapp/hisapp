//
//  MemberDetailTableViewCell.h
//  Restapp
//
//  Created by GAYATHIRIDEVI on 29/01/15.
//  Copyright (c) 2015 GAYATHIRIDEVI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblMemberName;
@property (strong, nonatomic) IBOutlet UILabel *lblMemberMobile;
@property (weak, nonatomic) IBOutlet UILabel *NumberList;
@end
