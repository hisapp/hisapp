//
//  ConatctViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 14/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "ConatctViewController.h"
#import "ContactTableViewCell.h"
#import "DBManager.h"

@interface ConatctViewController ()
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation ConatctViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    self.PhoneObject = [[PhoneBookObject alloc] init];
    self.ContactList = [[NSMutableArray alloc] init];
    
    
    if([self.PhoneObject CheckPhoneBookStatus])
        self.ContactList = [self.PhoneObject MArrayContactFunc];
    else
        [self ContactAlert];
    
    [self CombineContact];
    

    [super viewDidLoad];

}

-(void)CombineContact
{
    for(NSArray *cc in self.TempContact)
    {
        for(ContactDetail *dd in self.ContactList)
        {
            if([[cc objectAtIndex:1] isEqualToString:dd.mobileNumber])
            {
                dd.IsChecked = YES;
            }
        }
    }
}

-(void)ContactAlert
{
    UIAlertView *contact = [[UIAlertView alloc] initWithTitle:@"Access error" message:@"HisApp cannot access your Contacts. Kindly check in Settings -> Privacy -> Contacts." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [contact show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.ContactList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.contacts = (self.ContactList)[indexPath.row];
    static NSString *DetailCellIdentifier = @"ContactCell";
    ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:DetailCellIdentifier];
    [cell setDelegate:self];
    if (cell == nil) {
        cell = [[ContactTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DetailCellIdentifier] ;
    }
    cell.lblName.text = self.contacts.fullName;
    if(self.contacts.IsChecked)
    {
        [cell.imgVwTick setImage:[UIImage imageNamed:@"Checkmark.png"]];
    } else
    {
        [cell.imgVwTick setImage:[UIImage imageNamed:@""]];
    }
    cell.btnCeckBox.tag =indexPath.row;
    cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
    
}

-(void)buttonTappedOnCell:(ContactTableViewCell *)cell
{
    if (cell) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        ContactDetail *selectedModel = [self.ContactList objectAtIndex:indexPath.row];
        selectedModel.IsChecked = !selectedModel.IsChecked;
        [self.tableView reloadData];
    }
}

- (IBAction)DoneClick:(id)sender {
    
    NSMutableArray *SelectedContact = [[NSMutableArray alloc] init];
    
    for ( ContactDetail *contact in self.ContactList)
    {
        if(contact.IsChecked)
        {
            [SelectedContact addObject:contact];
            NSString *query;
            query = [NSString stringWithFormat:@"Select * from Contacts where CONTACTID=%@ ",contact.mobileNumber];
            
            if([self.dbManager recordExistOrNot:query])
            {
                query =[NSString stringWithFormat:@"Update contacts set archived ='no' where contactID=%@ " ,contact.mobileNumber];
                
                // Execute the query.
                [self.dbManager executeQuery:query];
                // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0)
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                else
                    NSLog(@"Could not execute the query.");
            }
            else
            {
                NSArray *aa = [NSArray arrayWithArray:contact.mobileNumbers];
                NSString *phNos = [aa componentsJoinedByString:@"|"];
                query =[NSString stringWithFormat:@"insert into contacts(contactID,contactName,contactNo,contactEmail,archived, IN_AMT, OUT_AMT )values(%@,'%@','%@','%@','no',0.00,0.00)",contact.mobileNumber,contact.fullName,phNos,contact.Email];
                
                // Execute the query.
                [self.dbManager executeQuery:query];
                // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0)
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                else
                    NSLog(@"Could not execute the query.");
            }
        }
    }
    [self.delegate addItemViewController:self didFinishEnteringItem:SelectedContact];
    [self.navigationController popViewControllerAnimated:YES];
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

@end



