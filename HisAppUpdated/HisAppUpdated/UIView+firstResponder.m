//
//  UIView+firstResponder.h
//  Hisaab
//
//  Created by GAYATHIRIDEVI on 27/02/15.
//  Copyright (c) 2015 GAYATHIRIDEVI. All rights reserved.
//

#import "UIView+firstResponder.h"

@implementation UIView (firstResponder)

- (UIView *)findFirstResponder
{
	if ([self isFirstResponder]) {
		return self;
	}
	
	for (UIView *subview in [self subviews]) {
		UIView *firstResponder = [subview findFirstResponder];
		if (nil != firstResponder) {
			return firstResponder;
		}
	}
	
	return nil;
}


@end
