//
//  SettlementViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 31/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettlementViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *OweSegment;

- (IBAction)OweSegmentClick:(id)sender;
@property(nonatomic,strong) NSString  *StrEventName;
@property(nonatomic,strong) NSString *StrEventId;
@property (strong, nonatomic) IBOutlet UITableView *tblResult;





@end
