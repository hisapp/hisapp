//
//  GroupTableViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 17/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "GroupTableViewController.h"
#import "AddGroupViewController.h"
#import "DBManager.h"


@interface GroupTableViewController ()
{
    NSString *ClickedGroupId, *ClickedGroupName;
}

@property(nonatomic,strong) DBManager *dbManager;
@end

@implementation GroupTableViewController
- (void)viewDidLoad{
    if(self.arrGroupsList == nil)
        self.arrGroupsList = [[NSMutableArray alloc]init];
    
    self.dbManager = [DBManager sharedObject];
    [self LoaddbData];
    [super viewDidLoad];
}



-(void)LoaddbData
{
   NSString *query = [NSString stringWithFormat:@"select * from Groups where archived ='no'  ORDER BY DATEMODIFIED DESC"];
    if(self.arrGroupsList == nil)
        self.arrGroupsList = [[NSMutableArray alloc]init];
    
    self.arrGroupsList =(NSMutableArray *)[self.dbManager loadDataFromDB:query];
    self. arrGroupsList = [self.arrGroupsList mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self LoaddbData];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.arrGroupsList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupList" forIndexPath:indexPath];
    // Configure the cell...
    
    cell.accessoryType =UITableViewCellAccessoryDisclosureIndicator;
    
    UILabel *lblGroupName = (UILabel *) [cell.contentView viewWithTag:1];
    lblGroupName.text =  [[self.arrGroupsList objectAtIndex:indexPath.row] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GROUPNAME"]];
    
    UILabel *lblGroupId = (UILabel *) [cell.contentView viewWithTag:2];
    lblGroupId.text =  [[self.arrGroupsList objectAtIndex:indexPath.row] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"GROUPID"]];
    

    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    UILabel *labelName = (UILabel *)[cell.contentView viewWithTag:1];
    ClickedGroupName= labelName.text;
    
    UILabel *labelId = (UILabel *)[cell.contentView viewWithTag:2];
    ClickedGroupId= labelId.text;
    
    [self performSegueWithIdentifier:@"ShowEditGroup" sender:self];
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        UILabel *label = (UILabel *)[cell.contentView viewWithTag:2];
        
        NSString *G_Id = label.text;
        
        [self.arrGroupsList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        NSString *query =[NSString stringWithFormat:@"UPDATE  groups set archived  = 'yes' where GROUPID ='%@'",G_Id];
        [self.dbManager executeQuery:query];
    }
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"ShowEditGroup"]){
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        AddGroupViewController *EditGroup = (AddGroupViewController *)navController.topViewController;
        EditGroup.StrNameGroup = ClickedGroupName;
        EditGroup.StrGroupId = ClickedGroupId;
        NSString *query = [NSString stringWithFormat:@"Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USERGROUPREL where groupid = %@)",ClickedGroupId];
    
        EditGroup.arrMembersList =  (NSMutableArray *) [self.dbManager loadDataByAscending:query index:0];
        EditGroup.strBtnName =@"Update";
    }
}



@end
