//
//  AppDelegate.h
//  HisAppUpdated
//
//  Created by GAYATHIRIDEVI on 09/05/15.
//  Copyright (c) 2015 Nyqmind. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MBProgressHUD.h"
#import "DBManager.h"
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    MBProgressHUD *hud;
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)ProgressView:(UIView *)InView Messsage:(NSString *)MsgStr;
-(void)HideProgressView;

- (BOOL)reachable;
@end

