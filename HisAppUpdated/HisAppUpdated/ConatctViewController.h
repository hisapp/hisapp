//
//  ConatctViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 14/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactTableViewCell.h"
#import "PhoneBookObject.h"
@class ConatctViewController;


@protocol SendContactDetail <NSObject>
- (void)addItemViewController:(ConatctViewController *)controller didFinishEnteringItem:(NSMutableArray *)SelectedContact;
@end

@interface ConatctViewController : UITableViewController<MyCustomCellDelegate>


@property(nonatomic) PhoneBookObject *PhoneObject;
@property(nonatomic) ContactDetail *contacts;
@property(nonatomic) NSMutableArray *ContactList;
@property(nonatomic) NSMutableArray *TempContact;

- (IBAction)DoneClick:(id)sender;
@property (weak) id <SendContactDetail> delegate;
@property (strong, nonatomic) IBOutlet UINavigationItem *NavBarItem;



@end
