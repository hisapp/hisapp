//
//  EventUserTableViewController.m
//  HisApp
//
//  Created by GAYATHIRIDEVI on 26/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import "EventUserTableViewController.h"
#import "DBManager.h"


@interface EventUserTableViewController ()
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation EventUserTableViewController

- (void)viewDidLoad {
    
    self.dbManager = [DBManager sharedObject];
    [self LoadDbData];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    
    
}

-(void)LoadDbData
{
    if(self.MembersList ==nil)
    {
        self.MembersList = [[NSMutableArray alloc] init];
    }
    NSString *query = [NSString stringWithFormat:@"Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USEREVENTREL where EVENTID =%@)",self.StrEventId];
    
//    Select CONTACTNAME ,CONTACTID from CONTACTS where contactid in (Select userid from USEREVENTREL where EVENTID =%@)
    self.MembersList = (NSMutableArray *)[self.dbManager loadDataByAscending:query index:0];
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.MembersList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventUserList" forIndexPath:indexPath];
    
    
    // Configure the cell...
    
    UILabel *lblEventID = (UILabel *) [cell.contentView viewWithTag:2];
    lblEventID.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:1];
    
    UILabel *lblEventName = (UILabel *) [cell.contentView viewWithTag:1];
    lblEventName.text = [[self.MembersList objectAtIndex:indexPath.row] objectAtIndex:0];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.MembersList removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)Home_CloseClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
