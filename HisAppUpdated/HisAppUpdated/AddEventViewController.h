//
//  AddEventViewController.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 23/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddEventViewController : UIViewController

- (IBAction)ShowDate:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtStartDate;
@property (strong, nonatomic) IBOutlet UITextField *txtEndDate;
@property (strong, nonatomic) IBOutlet UITextField *txtEventName;
@property (strong, nonatomic) IBOutlet UIButton *btnEndDate;
- (IBAction)btnNextClick:(id)sender;


@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)btnDateDone:(id)sender;
- (IBAction)btnDateClose:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblDatePickerTitle;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

@property (strong, nonatomic) IBOutlet UILabel *lblNotification;

@end
