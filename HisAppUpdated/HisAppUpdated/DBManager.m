//
//  DBManager.m
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>


static DBManager *dbObject;


@interface DBManager()

@property (nonatomic, strong) NSString *documentsDirectory;

@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic)sqlite3 *contactDB;

-(void)copyDatabaseIntoDocumentsDirectory;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

@end


@implementation DBManager

#pragma mark - Initialization
+ (DBManager*)sharedObject
{
    if(!dbObject)
    {
        dbObject = [[DBManager alloc] init];
    }
    return dbObject;
}



-(void)initWithDatabaseFilename:(NSString *)dbFilename{
   
        // Set the documents directory path to the documentsDirectory property.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        self.documentsDirectory = [paths objectAtIndex:0];
        
        
        // Build the path to the database file
        _databasePath = [[NSString alloc] initWithString: [self.documentsDirectory stringByAppendingPathComponent:dbFilename]];
    
    NSLog(@"%@",_databasePath);
        
        
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        
        if ([filemgr fileExistsAtPath: _databasePath ] == NO)
        {
            const char *dbpath = [_databasePath UTF8String];
            
            if (sqlite3_open(dbpath, &_contactDB) == SQLITE_OK)
            {
                
                NSLog(@"%@",self.documentsDirectory);
                char *errMsg;
                
//                const char *sql_stmt_1 = "CREATE TABLE  IF NOT EXISTS USERS(USERID LONG PRIMARY KEY UNIQUE ,FIRSTNAME VARCHAR(255),LASTNAME VARCHAR(255),NICKNAME VARCHAR(255),PHONENO LONG ,EMAIL VARCHAR(50),ALTERNATEEMAIL VARCHAR(50),UDID VARCHAR(100),STATUSID INT,DATECREATED TIMESTAMP,DATEDELETED TIMESTAMP,LASTMODIFIED TIMESTAMP)";
//                
//                if (sqlite3_exec(_contactDB, sql_stmt_1, NULL, NULL, &errMsg) == SQLITE_OK)
//                    NSLog(@" USERS Created");
//                else
//                    NSLog(@"USERS Not Created");
                
                
                
                const char *sql_stmt_2 = "CREATE TABLE  IF NOT EXISTS GROUPS(GROUPID INTEGER PRIMARY KEY AUTOINCREMENT,GROUPNAME VARCHAR(255),DATECREATED TIMESTAMP,DATEDELETED TIMESTAMP,DATEMODIFIED TIMESTAMP,ARCHIVED VARCHAR(10))";
                
                if (sqlite3_exec(_contactDB, sql_stmt_2, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"GROUPS Created");
                else
                    NSLog(@"GROUPS Not Created");
                
                const char *sql_stmt_3 = "CREATE TABLE  IF NOT EXISTS USERGROUPREL(USERID LONG , GROUPID INTEGER,FOREIGN KEY (GROUPID) REFERENCES GROUPS(GROUPID),UNIQUE(USERID,GROUPID))";
                
                if (sqlite3_exec(_contactDB, sql_stmt_3, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@" USERGROUPREL Created");
                else
                    NSLog(@"USERGROUPREL Not Created");
                
                
                
                const char *sql_stmt_4 = "CREATE TABLE CONTACTS(CONTACTID LONG PRIMARY KEY UNIQUE,CONTACTNAME VARCHAR(255),CONTACTNO VARCHAR(255),CONTACTEMAIL VARCHAR(255),ARCHIVED VARCHAR(10), IN_AMT INTEGER, OUT_AMT INTEGER)";
                
                if (sqlite3_exec(_contactDB, sql_stmt_4, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"CONTACTS Created");
                else
                    NSLog(@"CONTACTS Not Created");
                
                
                const char *sql_stmt_5 = "CREATE TABLE EVENTS(EVENTID INTEGER PRIMARY KEY AUTOINCREMENT,EVENTNAME VARCHAR(255),DATECREATED TIMESTAMP DEFAULT CURRENT_TIMESTAMP,DATEDELETED TIMESTAMP,STARTDATE VARCHAR(255),ENDDATE VARCHAR(255),EVENTADMIN LONG)";
                
                if (sqlite3_exec(_contactDB, sql_stmt_5, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"EVENTS Created");
                else
                    NSLog(@"EVENTS Not Created");
                
                
                
                const char *sql_stmt_6 = "CREATE TABLE USEREVENTREL(USERID LONG  ,EVENTID INTEGER , UNIQUE(USERID , EVENTID), FOREIGN KEY(USERID) REFERENCES CONTACTS(CONTACTID),FOREIGN KEY(EVENTID) REFERENCES EVENTS(EVENTID))";
                
                if (sqlite3_exec(_contactDB, sql_stmt_6, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"USEREVENTREL Created");
                else
                    NSLog(@"USEREVENTREL Not Created");
                
                
                const char *sql_stmt_7 = "CREATE TABLE EVENTEXPENSES(EXPENSEID INTEGER PRIMARY KEY AUTOINCREMENT,EXPENSENAME VARCHAR(255),AMOUNT INTEGER,EXPENSEOWNERID LONG , EVENTID INTEGER NOT NULL, DIVIDETYPE INTEGER,FOREIGN KEY(EVENTID) REFERENCES EVENTS(EVENTID))";
                
                if (sqlite3_exec(_contactDB, sql_stmt_7, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"EVENTEXPENSES Created");
                else
                    NSLog(@"EVENTEXPENSES Not Created");
                
                
                const char *sql_stmt_8 = "CREATE TABLE EXPENSESTATUS(STATUSNAME VARCHAR(20), STATUSID INT )";
                
                if (sqlite3_exec(_contactDB, sql_stmt_8, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"EXPENSESTATUS Created");
                else
                    NSLog(@"EXPENSESTATUS Not Created");
                
                const char *sql_stmt_9 = "CREATE TABLE EXPENSEDETAIL(PAYER LONG, TO_WHOM LONG , AMOUNT VARCHAR(30), EXPENSEID INTEGER, STATUSID INTEGER ,FOREIGN KEY(STATUSID) REFERENCES EXPENSESTATUS(STATUSID))";
                
                if (sqlite3_exec(_contactDB, sql_stmt_9, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"EXPENSEDETAIL Created");
                else
                    NSLog(@"EXPENSEDETAIL Not Created");
                
                
                const char *sql_stmt_10 = " CREATE TRIGGER GropModified  UPDATE ON groups  BEGIN Update  GROUPS set datemodified =CURRENT_TIMESTAMP where groupid= new.groupid ;  END ";
                
                if (sqlite3_exec(_contactDB, sql_stmt_10, NULL, NULL, &errMsg) == SQLITE_OK)
                    NSLog(@"GropModified trigger Created");
                else
                    NSLog(@"GropModified trigger Not Created");
                

              
                
                
                
                sqlite3_close(_contactDB);
            }
            
            else {
                NSLog(@"DB Not Created");
            }
        }
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Copy the database file into the documents directory if necessary.
        //        [self copyDatabaseIntoDocumentsDirectory];

  
}


#pragma mark - Private method implementation

-(void)copyDatabaseIntoDocumentsDirectory{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}



-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
	// Create a sqlite object.
	sqlite3 *sqlite3Database;
	
    // Set the database file path.
	NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
	self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    
	// Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
	if(openDatabaseResult == SQLITE_OK) {
		// Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
		sqlite3_stmt *compiledStatement;
		
        // Load all data from database to memory.
        BOOL prepareStatementResult = sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL);
		if(prepareStatementResult == SQLITE_OK) {
			// Check if the query is non-executable.
			if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
				// Loop through the results and add them to the results array row by row.
				while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
					// Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
					for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
						char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
						if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
							[arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
						}
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
					
					// Store each fetched data row in the results array, but first check if there is actually data.
					if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
					}
				}
			}
			else {
                // This is the case of an executable query (insert, update, ...).
                
				// Execute the query.
//                BOOL executeQueryResults = sqlite3_step(compiledStatement);
                if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
				}
				else {
					// If could not execute the query show the error message on the debugger.
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
				}
			}
		}
		else {
            // In the database cannot be opened then show the error message on the debugger.
			NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
		}
		
		// Release the compiled statement from memory.
		sqlite3_finalize(compiledStatement);
		
	}
    
    // Close the database.
	sqlite3_close(sqlite3Database);
}


#pragma mark - Public method implementation

-(NSArray *)loadDataFromDB:(NSString *)query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

-(NSArray *)loadDataByAscending:(NSString *)query index:(NSUInteger)Value
{
     [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    [self.arrResults sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSArray *array1 = (NSArray *)obj1;
        NSArray *array2 = (NSArray *)obj2;
        NSString *num1String = [array1 objectAtIndex:Value];
        NSString *num2String = [array2 objectAtIndex:Value];
        
        return [num1String compare:num2String];
    }];
    
    return (NSArray *)self.arrResults;
}


-(void)executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}


-(BOOL)recordExistOrNot:(NSString *)query{
    BOOL recordExist=NO;
    
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];

    if(sqlite3_open([databasePath UTF8String], &sqlite3Database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(sqlite3Database, [query UTF8String], -1, &statement, nil)==SQLITE_OK)
        {
            if (sqlite3_step(statement)==SQLITE_ROW)
            {
                recordExist=YES;
            }
            else
            {
                NSLog(@"%s,",sqlite3_errmsg(sqlite3Database));
            }
            sqlite3_finalize(statement);
            sqlite3_close(sqlite3Database);
        }
    }
    return recordExist;
}

-(NSString *)Stringvanish:(NSString *)incorrect
{
    NSString *unfilteredString = incorrect;
    NSCharacterSet *AllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *resultString = [[unfilteredString componentsSeparatedByCharactersInSet:AllowedChars] componentsJoinedByString:@""];
    return resultString;
}




@end
