//
//  PhoneBookObject.h
//  HisApp
//
//  Created by GAYATHIRIDEVI on 24/03/15.
//  Copyright (c) 2015 HisApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import "ContactDetail.h"



@interface PhoneBookObject : NSObject

@property(nonatomic) CFErrorRef *error ;
@property(nonatomic) ABAddressBookRef addressBook ;

@property(nonatomic) NSMutableArray *ContactList;
@property(nonatomic) ContactDetail *contacts;
@property(assign)    bool status;;



-(NSMutableArray *)MArrayContactFunc;
-(BOOL)CheckPhoneBookStatus;


@end
