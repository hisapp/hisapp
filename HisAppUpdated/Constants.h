//
//  Constants.h
//  SPFFoods
//
//  Created by DURGA PRASAD on 22/02/14.
//  Copyright (c) 2014 SPFSolutions, LLC. All rights reserved.
//

#ifndef SPFFoods_Constants_h

#ifdef      UI_USER_INTERFACE_IDIOM
#define     IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#else
#define     IS_IPHONE false
#endif

#define     iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define     DEFAULT_ROW_HEIGHT          40
#define     HEADER_HEIGHT               50


#define     urlRegistration           @"http://skillconnect.in/Hisapp/index.php/registration"
#define     urlCodeActivation         @"http://skillconnect.in/Hisapp/index.php/confirmreg/getActivate"
#define     urlSendContacts           @"http://skillconnect.in/Hisapp/index.php/contactDetails"


#endif


//{
//    "error": {
//        "message": "Already Registered"
//    }
//}
//{
//    "error": {
//        "message": "Invalid key"
//    }
//}